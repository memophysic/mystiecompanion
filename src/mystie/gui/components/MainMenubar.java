/**
 * Class for the main frame's menubar
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components;

import javax.swing.BorderFactory;

import mystie.gui.components.actions.ActEditMenu;
import mystie.gui.components.actions.ActFileMenu;
import mystie.gui.components.actions.ActViewMenu;
import mystie.gui.components.menu.BaseMenu;

import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.hotkey.Hotkey;

@SuppressWarnings("serial")
public class MainMenubar extends WebMenuBar
{
	public final int TOOLTIP_TRAILING = 100;
	
	private WebFrame m_oParentFrame;
	
	private WebMenu m_oFileMenu;
	private WebMenu m_oEditMenu;
	private WebMenu m_oViewMenu;
	
	public MainMenubar(WebFrame parentFrame)
	{
		super();
		m_oParentFrame = parentFrame;
		
		initializeLayout();
		initializeMenuItems();
	}
	
	/**
	 * Initialize the menubar layout
	 */
	public void initializeLayout()
	{
		setUndecorated(true);
		setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
	}
	
	/**
	 * Initialize the menubar's different items
	 */
	public void initializeMenuItems()
	{
		initializeFileMenuItems();
		initializeEditMenuItems();
		initializeViewMenuItems();
	}
	
	/**
	 * Initialize the "file" menu
	 */
	private void initializeFileMenuItems()
	{
		m_oFileMenu = new BaseMenu("File");
		add(m_oFileMenu);
		
		m_oFileMenu.add(new ActFileMenu.NewProject(m_oParentFrame,
				"New Project", "Create a new game project", Hotkey.CTRL_N));
		
		m_oFileMenu.add(new ActFileMenu.OpenProject(m_oParentFrame,
				"Open Project...", "Open an existing project", Hotkey.CTRL_O));
		
		m_oFileMenu.add(new ActFileMenu.CloseProject(m_oParentFrame,
				"Close Project", "Close the currently opened project", null));
		
		m_oFileMenu.addSeparator();
		
		m_oFileMenu.add(new ActFileMenu.SaveProject(m_oParentFrame,
				"Save project", "Save the current project", Hotkey.CTRL_S));
		
		m_oFileMenu.addSeparator();
		
		m_oFileMenu.add(new ActFileMenu.Preferences(m_oParentFrame,
				"Preferences...", "Open the preferences dialog",
				Hotkey.CTRL_I));
		
		m_oFileMenu.addSeparator();
		
		m_oFileMenu.add(new ActFileMenu.Quit(m_oParentFrame,
				"Quit", "Quit the software", Hotkey.CTRL_Q));
	}
	
	/**
	 * Initialize the "edit" menu
	 */
	private void initializeEditMenuItems()
	{
		m_oEditMenu = new BaseMenu("Edit");
		add(m_oEditMenu);
		
		m_oEditMenu.add(new ActEditMenu.Cut(m_oParentFrame,
				"Cut", null, Hotkey.CTRL_X));
		
		m_oEditMenu.add(new ActEditMenu.Copy(m_oParentFrame,
				"Copy", null, Hotkey.CTRL_C));
		
		m_oEditMenu.add(new ActEditMenu.Paste(m_oParentFrame,
				"Paste", null, Hotkey.CTRL_V));
	}
	
	/**
	 * Initialize the "view" menu
	 */
	private void initializeViewMenuItems()
	{
		m_oViewMenu = new BaseMenu("View");
		add(m_oViewMenu);
		
		m_oViewMenu.add(new ActViewMenu.Fullscreen(m_oParentFrame,
				"Toggle Fullscreen", 
				"Maximize the window to fill the entire screen",
				Hotkey.F11));
	}
}
