/**
 * Class for the main window frame of the application.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components;

import java.io.File;

import javax.swing.SwingConstants;

import mystie.core.project.Project;

import com.alee.extended.painter.BorderPainter;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.splitpane.WebSplitPane;

@SuppressWarnings("serial")
public class MainPanel extends WebPanel implements SwingConstants
{
	private WebFrame m_oParentFrame;
	private SidebarPanel m_oSidebar;
	private WorkPanel m_oWorkPane;
	private WebSplitPane m_oSplitPane;
	
	@SuppressWarnings("rawtypes")
	public MainPanel(WebFrame parentFrame)
	{
		super();
		m_oParentFrame = parentFrame;
        setPainter(new BorderPainter());
        setMargin(4);
        
        m_oSidebar = new SidebarPanel(m_oParentFrame);
        m_oWorkPane = new WorkPanel(m_oParentFrame);
        
        m_oSplitPane = new WebSplitPane(
        		WebSplitPane.HORIZONTAL_SPLIT, m_oSidebar, m_oWorkPane);
        m_oSplitPane.setOneTouchExpandable(true);
        m_oSplitPane.setContinuousLayout(true);
        
        add(m_oSplitPane);
	}
	
	public void initializeComponents()
	{
		m_oSplitPane.setDividerLocation(0.25);
	}
	
	/**
	 * @param oPrj project to be loaded, can be null for removal of any loaded 
	 * project
	 * @return true if a project was successfully loaded/unloaded, 
	 * false otherwise
	 */
	public boolean loadProject(Project oPrj)
	{
		File root = oPrj != null ? oPrj.getProjectRoot() : null;
		
		return m_oSidebar.setProjectRoot(root) == (oPrj != null);
	}
}
