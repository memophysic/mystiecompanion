/**
 * Base tree menu item for popup menu
 */

package mystie.gui.components.menu;

import javax.swing.ImageIcon;

import com.alee.extended.tree.AsyncUniqueNode;
import com.alee.extended.tree.WebAsyncTree;

@SuppressWarnings("serial")
public abstract class TreeMenuItem<E extends AsyncUniqueNode>
	extends BaseMenuItem
{
	private final WebAsyncTree<E> m_oTree;
	private final E m_oNode;
	
	public TreeMenuItem(WebAsyncTree<E> oTree, E oNode,
			String sTitle, String sTooltip)
	{
		super(sTitle, sTooltip, null);
		m_oTree = oTree;
		m_oNode = oNode;
	}
	
	public TreeMenuItem(WebAsyncTree<E> oTree, E oNode, String sTitle)
	{
		this(oTree, oNode, sTitle, (String)null);
	}
	
	public TreeMenuItem(WebAsyncTree<E> oTree, E oNode,
			String sTitle, String sTooltip, ImageIcon icn)
	{
		this(oTree, oNode, sTitle, sTooltip);
		setIcon(icn);
	}
	
	public TreeMenuItem(WebAsyncTree<E> oTree, E oNode,
			String sTitle, ImageIcon icn)
	{
		this(oTree, oNode, sTitle, null, icn);
	}
	
	public WebAsyncTree<E> getTree()
	{
		return m_oTree;
	}
	
	public E getNode()
	{
		return m_oNode;
	}
}