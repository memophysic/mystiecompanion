/**
 * Base menu item for a frame
 */

package mystie.gui.components.menu;

import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.hotkey.HotkeyData;

@SuppressWarnings("serial")
public abstract class FrameMenuItem extends BaseMenuItem
{
	private WebFrame m_oMainFrame;
	
	public FrameMenuItem(WebFrame oMainFrame,
			String sTitle, String sTooltip, HotkeyData hotkey)
	{
		super(sTitle, sTooltip, hotkey);
		
		m_oMainFrame = oMainFrame;
	}
	
	public WebFrame getParentFrame()
	{
		return m_oMainFrame;
	}
}