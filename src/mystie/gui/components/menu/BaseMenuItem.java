/**
 * Base menu item with conditional checks done 
 */

package mystie.gui.components.menu;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

import mystie.gui.ResourcesManager;
import mystie.gui.components.ConditionalEnable;

import com.alee.laf.menu.WebMenuItem;
import com.alee.managers.hotkey.HotkeyData;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;

@SuppressWarnings("serial")
public abstract class BaseMenuItem extends WebMenuItem
	implements ConditionalEnable, ActionListener
{
	public final int TOOLTIP_TRAILING = 100;
	public final static int ICON_SIZE_DEFAULT = 16; // px
	
	public BaseMenuItem(String sTitle, String sTooltip, HotkeyData hotkey)
	{
		super(sTitle);
		
		addActionListener(this);
		
		if(hotkey != null)
			setAccelerator(hotkey);
		
		if(sTooltip != null)
			TooltipManager.setTooltip(this, 
				sTooltip, TooltipWay.trailing, TOOLTIP_TRAILING);
	}
	
	public BaseMenuItem(String sTitle, String sTooltip, HotkeyData hotkey,
			ImageIcon icn)
	{
		this(sTitle, sTooltip, hotkey);
		setIcon(icn);
	}
	
	@Override
	public void updateEnable()
	{
		setEnabled(getConditionalEnable());
	}
	
	@Override
	public boolean getConditionalEnable()
	{
		return true;
	}
	
	/**
	 * Set an ImageIcon object as an icon resized to default size
	 * @param icn ImageIcon to be set
	 */
	public void setIcon(ImageIcon icn)
	{
		setIcon(icn, ICON_SIZE_DEFAULT);
	}
	
	/**
	 * Set an ImageIcon object as an icon resized to specified size
	 * @param icn ImageIcon to be set
	 * @param size length in pixel of each side of the icon
	 */
	public void setIcon(ImageIcon icn, int size)
	{
		if(icn != null)
			if(icn.getIconWidth() != size || icn.getIconHeight() != size)
			{
				icn = new ImageIcon(ResourcesManager.resizeImage(
						icn.getImage(), size, size));
			}
		super.setIcon(icn);
	}
}