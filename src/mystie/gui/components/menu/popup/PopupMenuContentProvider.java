/**
 * Content provider for a dynamic popup menu
 */

package mystie.gui.components.menu.popup;

import java.util.List;

import javax.swing.JComponent;

public interface PopupMenuContentProvider
{
	public List<JComponent> getPopupMenuContent();
}
