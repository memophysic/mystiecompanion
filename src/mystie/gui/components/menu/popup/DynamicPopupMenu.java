package mystie.gui.components.menu.popup;

import java.awt.Component;
import java.util.List;

import javax.swing.JComponent;

import mystie.gui.components.ConditionalEnable;

import com.alee.laf.menu.WebPopupMenu;
import com.alee.managers.style.skin.web.PopupStyle;

@SuppressWarnings("serial")
public class DynamicPopupMenu extends WebPopupMenu
{
	/**
	 * Dynamic popup menu constructor
	 */
	public DynamicPopupMenu()
	{
		super();
		setPopupStyle(PopupStyle.simple);
	}
	
	/**
	 * Add a list of JComponent content to the popup menu
	 * @param content
	 */
	private void addComponents(List<JComponent> content)
	{
		for(JComponent oCon : content)
		{
			if(oCon instanceof ConditionalEnable)
				((ConditionalEnable)oCon).updateEnable();
			add(oCon);
		}
	}
	
	/**
	 * Update the popup menu content dynamically using a content provider while
	 * adding additional content from a provided list of components
	 * 
	 * @param customContent custom content to add to the popup menu
	 * @param oContentPrv provider of menu content
	 * @return true if the popup content was updated to a non-empty list of
	 * components, false otherwise
	 */
	public boolean updatePopupContent(List<JComponent> customContent,
			PopupMenuContentProvider oContentPrv)
	{
		boolean bSuccess = updatePopupContent(oContentPrv);
		if(bSuccess)
			addSeparator();
		addComponents(customContent);
		
		return bSuccess || !customContent.isEmpty();
	}
	
	/**
	 * Update the popup menu content dynamically based on the one provided by
	 * the passed provider object
	 * @param oContentPrv provider of menu content¸
	 * @return true if the popup content was updated to a non-empty list of
	 * components, false otherwise
	 */
	private boolean updatePopupContent(PopupMenuContentProvider oContentPrv)
	{
		removeAll();
		List<JComponent> content = oContentPrv.getPopupMenuContent();
		addComponents(content);
		return !content.isEmpty();
	}
	
	/**
	 * Dynamically change content of the popup menu based on the one provided
	 * by the provider passed as argument prior to showing the menu.<br>
	 * Skips showing the popup menu if no content is provided from the provider
	 * 
	 * @param oContentPrv provider of menu content
	 * @param invoker the component in whose space the popup menu is to appear
     * @param x the x coordinate in invoker's coordinate space at which
     * the popup menu is to be displayed
     * @param y the y coordinate in invoker's coordinate space at which
     * the popup menu is to be displayed
	 */
	public void show(PopupMenuContentProvider oContentPrv,
			Component invoker, int x, int y)
	{
		if(!updatePopupContent(oContentPrv))
			return;
		
		super.show(invoker, x, y);
	}
	
	/**
	 * Dynamically change content of the popup menu based on the one provided
	 * by the provider passed as argument and the custom content provided by
	 * the caller prior to showing the menu.<br>
	 * Skips showing the popup menu if no content is provided from the provider
	 * and passed as custom content
	 * 
	 * @param oContentPrv provider of menu content
	 * @param customContent custom content to add to the popup menu
	 * @param invoker the component in whose space the popup menu is to appear
     * @param x the x coordinate in invoker's coordinate space at which
     * the popup menu is to be displayed
     * @param y the y coordinate in invoker's coordinate space at which
     * the popup menu is to be displayed
	 */
	public void show(PopupMenuContentProvider oContentPrv,
			List<JComponent> customContent,
			Component invoker, int x, int y)
	{
		if(!updatePopupContent(customContent, oContentPrv))
			return;
		
		super.show(invoker, x, y);
	}
}
