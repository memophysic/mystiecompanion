/**
 * Base menu which can use items implementing the ConditionalEnable interface
 * to update their enable state upon action on the menu
 */

package mystie.gui.components.menu;

import java.awt.Component;

import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import mystie.gui.components.ConditionalEnable;

import com.alee.laf.menu.WebMenu;

@SuppressWarnings("serial")
public class BaseMenu extends WebMenu
{
	public BaseMenu(String sTitle)
	{
		super(sTitle);
		addMenuListener(new UpdateMenuListener());
	}
	
	private class UpdateMenuListener implements MenuListener
	{
		@Override
		public void menuSelected(MenuEvent e)
		{
			for(Component com : getMenuComponents())
				if(com instanceof ConditionalEnable)
					((ConditionalEnable) com).updateEnable();
		}

		@Override
		public void menuDeselected(MenuEvent e){}
		@Override
		public void menuCanceled(MenuEvent e){}
	}
}
