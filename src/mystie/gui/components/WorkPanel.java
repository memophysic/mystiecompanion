/**
 * Class for the work region of the application
 */

package mystie.gui.components;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;

@SuppressWarnings("serial")
public class WorkPanel extends WebPanel
{
	WebFrame m_oParentFrame;
	
	public WorkPanel(WebFrame parentFrame)
	{
		m_oParentFrame = parentFrame;
	}
}
