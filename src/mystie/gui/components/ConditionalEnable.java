package mystie.gui.components;

public interface ConditionalEnable
{
	/**
	 * Resolve conditional statement on which the enable state is based on
	 * 
	 * @return true if instance should be enabled, false otherwise
	 */
	public boolean getConditionalEnable();
	
	/**
	 * Update the enable state from the conditional enable value from
	 * method getConditionalEnable
	 */
	public void updateEnable();
}
