/**
 * Tree for the project hierarchy which 
 */

package mystie.gui.components.prjtree;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import mystie.gui.components.actions.ActPrjTree;
import mystie.gui.components.menu.popup.DynamicPopupMenu;
import mystie.gui.components.prjtree.node.ProjectNode;

import com.alee.extended.tree.AsyncTreeDataProvider;
import com.alee.extended.tree.WebAsyncTree;

@SuppressWarnings("serial")
public class ProjectTree extends WebAsyncTree<ProjectNode> 
	implements MouseListener
{
	DynamicPopupMenu m_oPopupMenu;
	
	public ProjectTree(AsyncTreeDataProvider<ProjectNode> dataProvider)
	{
		super(dataProvider);
		m_oPopupMenu = new DynamicPopupMenu();
		addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	    if (SwingUtilities.isRightMouseButton(e))
	    {
	        int row = getClosestRowForLocation(e.getX(), e.getY());
	        setSelectionRow(row);
	        if(row != -1)
	        {
		        ProjectNode node = getNodeForRow(row);
		        if(node != null)
			        m_oPopupMenu.show(node, createPopupContent(node),
			        		e.getComponent(), e.getX(), e.getY());
	        }
	    }
	}
	
	private Vector<JComponent> createPopupContent(ProjectNode node)
	{
		Vector<JComponent> popupContent = new Vector<JComponent>();
        popupContent.add(
        		new ActPrjTree.RefreshTreeMenuItem<ProjectNode>(this, node));
        return popupContent;
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}
}
