/**
 * Tree model for the project
 */

package mystie.gui.components.prjtree;

import java.io.File;
import java.util.Comparator;
import java.util.Vector;

import mystie.core.project.content.ContentHierarchy;
import mystie.gui.components.prjtree.node.ProjectNode;
import mystie.gui.components.prjtree.node.ProjectNodeOverworld;
import mystie.gui.components.prjtree.node.ProjectNodeType;

import com.alee.extended.tree.AsyncTreeDataProvider;
import com.alee.extended.tree.ChildsListener;
import com.alee.utils.compare.Filter;

public class ProjectTreeDP implements AsyncTreeDataProvider<ProjectNode>
{
	final File m_oPrjRoot;
	
	public ProjectTreeDP(File oPrjRoot)
	{
		super();
		m_oPrjRoot = oPrjRoot;
	}
	
	@Override
	public ProjectNode getRoot()
	{
		return new ProjectNode ("Project", ProjectNodeType.ROOT);
	}

	@Override
	public void loadChilds(ProjectNode parent,
			ChildsListener<ProjectNode> listener)
	{
		boolean bSuccess = true;
		Vector<ProjectNode> nodeList = new Vector<ProjectNode>();
		
        switch (parent.getType())
        {
        	case ROOT:
        	{
                nodeList.add(new ProjectNodeOverworld());
                nodeList.add(new ProjectNode(
        				"Items", ProjectNodeType.ITEM));
                nodeList.add(new ProjectNode(
        				"Menus", ProjectNodeType.MENU));
        		break;
        	}
        	case OVERWORLD:
        	{
        		File[] fileList = ContentHierarchy.getDirList(m_oPrjRoot,
        				ContentHierarchy.DIRNAME_OVERWORLD);
        		
        		bSuccess = fileList != null;
        		
        		if(bSuccess)
	        		for(File oFile : fileList)
	        			nodeList.add(new ProjectNode(
	            			oFile.getName(), ProjectNodeType.OVERWORLD_ENTRY));
        		break;
        	}
        	case ITEM:
        	{
        		nodeList.add(new ProjectNode(
        				"NPCs", ProjectNodeType.NPC));
        		nodeList.add(new ProjectNode(
        				"Block Tiles", ProjectNodeType.BLOCK));
        		break;
        	}
        	case NPC:
        	{
        		nodeList.add(new ProjectNode(
        				"Friendly", ProjectNodeType.FRIENDLY));
        		nodeList.add(new ProjectNode(
        				"Hostile", ProjectNodeType.HOSTILE));
        		break;
        	}
        	default:
        		listener.childsLoadFailed(null);
        		return;
        }
        
        if(bSuccess)
        	listener.childsLoadCompleted(nodeList);
        else
        	listener.childsLoadFailed(null);
	}

	@Override
	public Comparator<ProjectNode> getChildsComparator(ProjectNode node)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Filter<ProjectNode> getChildsFilter(ProjectNode node)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isLeaf(ProjectNode node)
	{
		return node.getType().equals(ProjectNodeType.LEAF);
	}
}
