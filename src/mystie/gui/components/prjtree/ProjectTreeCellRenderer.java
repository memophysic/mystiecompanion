package mystie.gui.components.prjtree;

import javax.swing.ImageIcon;
import javax.swing.JTree;

import mystie.gui.components.prjtree.node.ProjectNode;

import com.alee.extended.tree.WebAsyncTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import com.alee.laf.tree.WebTreeUI;

@SuppressWarnings("serial")
public class ProjectTreeCellRenderer extends WebAsyncTreeCellRenderer
{
    /**
     * Returns custom tree cell renderer component
     *
     * @param tree       tree
     * @param value      cell value
     * @param isSelected whether cell is selected or not
     * @param expanded   whether cell is expanded or not
     * @param leaf       whether cell is leaf or not
     * @param row        cell row number
     * @param hasFocus   whether cell has focusor not
     * @return renderer component
     */
    @Override
    public WebTreeElement getTreeCellRendererComponent(
    		final JTree tree, final Object value, final boolean isSelected,
        	final boolean expanded, final boolean leaf, final int row,
        	final boolean hasFocus)
    {
        super.getTreeCellRendererComponent(
        		tree, value, isSelected, expanded, leaf, row, hasFocus);

        if(value instanceof ProjectNode)
        {
            final ProjectNode node = (ProjectNode)value;

            // Node icon
            if (!node.isLoading())
            {
                // Type icon
                final ImageIcon icon;
                switch (node.getType())
                {
                    case ROOT:
                    {
                        icon = WebTreeUI.ROOT_ICON;
                        break;
                    }
                    case LEAF:
                    {
                        icon = WebTreeUI.LEAF_ICON;
                        break;
                    }
                    default:
                    {
                    	icon = expanded ? WebTreeUI.OPEN_ICON:
          				  				  WebTreeUI.CLOSED_ICON;
                    	break;
                    }
                }
                setIcon (node.isFailed() ? getFailedStateIcon(icon): icon );
            }
            
            // Node text
            setText (node.getTitle());
        }

        return this;
    }
}