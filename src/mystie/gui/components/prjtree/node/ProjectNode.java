/**
 * Project node for the tree
 */
package mystie.gui.components.prjtree.node;

import java.util.List;
import java.util.Vector;

import javax.swing.JComponent;

import mystie.gui.components.menu.popup.PopupMenuContentProvider;
import mystie.gui.components.prjtree.ProjectTreeDP;

import com.alee.extended.tree.AsyncUniqueNode;

@SuppressWarnings("serial")
public class ProjectNode extends AsyncUniqueNode 
	implements PopupMenuContentProvider
{
	private final String m_sTitle;
	private final ProjectNodeType m_eType;
	
	/**
	 * Construct a project node destined to be used with ProjectTreeDP
	 * @param sTitle name to be displayed for the node
	 * @param eType type of the node
	 * @see ProjectTreeDP
	 */
	public ProjectNode(String sTitle, ProjectNodeType eType)
	{
		super();
		m_sTitle = sTitle;
		m_eType = eType;
	}
	
	/**
	 * @return title of the node
	 */
	public String getTitle()
	{
		return m_sTitle;
	}
	
	/**
	 * @return type of the node
	 */
	public ProjectNodeType getType()
	{
		return m_eType;
	}
	
	/**
	 * Default components for the popup menu
	 */
	@Override
	public List<JComponent> getPopupMenuContent()
	{
		return new Vector<JComponent>(); // empty
	}
}
