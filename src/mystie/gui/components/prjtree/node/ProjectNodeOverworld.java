/**
 * Project node for the tree
 */
package mystie.gui.components.prjtree.node;

import java.util.List;

import javax.swing.JComponent;

import mystie.gui.components.prjtree.ProjectTreeDP;

import com.alee.laf.menu.WebMenuItem;

@SuppressWarnings("serial")
public class ProjectNodeOverworld extends ProjectNode
{
	public static final String TITLE = "Overworld";
	
	/**
	 * Construct an overworld project node
	 * @see ProjectTreeDP
	 */
	public ProjectNodeOverworld()
	{
		super(TITLE, ProjectNodeType.OVERWORLD);
	}
	
	/**
	 * @return popup menu's content as JComponents list for the node
	 */
	@Override
	public List<JComponent> getPopupMenuContent()
	{
		List<JComponent> ret = super.getPopupMenuContent();
		ret.add(new WebMenuItem("Create Overworld"));
		
		return ret;
	}
}
