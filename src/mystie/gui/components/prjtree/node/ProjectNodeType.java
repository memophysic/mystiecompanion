package mystie.gui.components.prjtree.node;

public enum ProjectNodeType {
	ROOT,
	OVERWORLD,
	OVERWORLD_ENTRY,
	MAP,
	ITEM,
	NPC,
	FRIENDLY,
	HOSTILE,
	BLOCK,
	MENU,
	LEAF,
}
