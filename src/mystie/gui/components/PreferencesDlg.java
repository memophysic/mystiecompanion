/**
 * Dialog displaying the different preferences settings for the application
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;

import mystie.configuration.ConfigManager;
import mystie.configuration.ConfigVariable;
import mystie.gui.PresenterGuiConfig;

import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.text.WebFormattedTextField;

@SuppressWarnings("serial")
public class PreferencesDlg extends WebDialog
{
	private ConfigManager m_oConfMng;
	private PresenterGuiConfig m_oPreferencesConfig;
	private WebPanel m_oConfigPanel;
	private WebButton m_oBtnOk, m_oBtnCancel;
	
	private HashMap<String, WebFormattedTextField> m_hmFields = 
			new HashMap<String, WebFormattedTextField>();
	
	private HashMap<String, WebCheckBox> m_hmOptions = 
			new HashMap<String, WebCheckBox>();
	
	public static String WINDOW_TITLE = "Preferences";
	
	public PreferencesDlg(Component parent, ConfigManager configMng)
	{
		super(parent);
		
		m_oConfMng = configMng;
		assert(m_oConfMng.getConfigClass() == PresenterGuiConfig.class);
		m_oPreferencesConfig = (PresenterGuiConfig)m_oConfMng.getConfig();
		assert(m_oPreferencesConfig != null);
		
		m_oConfigPanel = new WebPanel();
		
		setupDialog(parent);
		loadConfigInterface();
	}
	
	/**
	 * Setup dialog properties
	 * 
	 * @param parent parent component
	 */
	private void setupDialog(Component parent)
	{
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(true);
		setShowResizeCorner(true);
		setSize(250, 400);
		setLocationRelativeTo(parent);
		setTitle(WINDOW_TITLE);
		
		WebPanel contentPanel = new WebPanel();
		contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		add(contentPanel);
		
		m_oConfigPanel.setLayout(
				new BoxLayout(m_oConfigPanel, BoxLayout.Y_AXIS));
		contentPanel.add(m_oConfigPanel, BorderLayout.CENTER);
		contentPanel.add(createButtonsPanel(), BorderLayout.SOUTH);
	}
	
	/**
	 * Create button panel
	 */
	private WebPanel createButtonsPanel()
	{
		WebPanel oBtnPanel = new WebPanel();
		oBtnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		m_oBtnOk = new WebButton("Ok");
		m_oBtnOk.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		    	saveAndClose();
		    }});
		m_oBtnOk.setSize(90, 30);
		m_oBtnOk.setBottomBgColor(new Color(130,170,240));
		oBtnPanel.add(m_oBtnOk);
		
		m_oBtnCancel = new WebButton("Cancel");
		m_oBtnCancel.setSize(90, 30);
		m_oBtnCancel.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		    	closeDlg();
		    }});
		oBtnPanel.add(m_oBtnCancel);
		
		return oBtnPanel;
	}

	/**
	 * Load the preferences from the configuration file into the interface
	 */
	private void loadConfigInterface()
	{
		for(String key: PresenterGuiConfig.FIELD_KEYS)
		{
			ConfigVariable var = m_oPreferencesConfig.getVar(key);
			if(var != null)
				addVariable(var);
		}
	}
	
	/**
	 * Add variable with its label
	 * 
	 * @param label label of the variable
	 * @param variable variable value
	 */
	private void addVariable(ConfigVariable variable)
	{
		@SuppressWarnings("rawtypes")
		Class type = variable.getType();
		
		if(type == Integer.class)
		{
			WebPanel panel = new WebPanel();
			panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			
			WebLabel text = new WebLabel(variable.getLabel());
			WebFormattedTextField field = 
					new WebFormattedTextField(variable.getValue());
			m_hmFields.put(variable.getKeyCode(), field);
			
			panel.add(text);
			panel.add(field);
			
			panel.setMaximumSize(new Dimension(200, 40));
			panel.setAlignmentX(WebPanel.RIGHT_ALIGNMENT);
			m_oConfigPanel.add(panel);
		}
		else if(type == Boolean.class)
		{
			WebPanel panel = new WebPanel();
			panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			
			WebLabel text = new WebLabel(variable.getLabel());
			
			WebCheckBox checkBox = new WebCheckBox();
			checkBox.setSelected((boolean)variable.getValue());
			m_hmOptions.put(variable.getKeyCode(), checkBox);
			
			panel.add(text);
			panel.add(checkBox);
			
			panel.setMaximumSize(new Dimension(200, 40));
			panel.setAlignmentX(WebPanel.RIGHT_ALIGNMENT);
			m_oConfigPanel.add(panel);
		}
	}
	
	/**
	 * Close and save the settings in their respective variables
	 * 
	 */
	private void saveAndClose()
	{
		// Fields
		for(String key: m_hmFields.keySet())
		{
			WebFormattedTextField field = m_hmFields.get(key);
			ConfigVariable variable = m_oPreferencesConfig.getVar(key);
			if(variable != null && field != null)
				variable.setValue(field.getValue());
		}
		
		// Options
		for(String key: m_hmOptions.keySet())
		{
			WebCheckBox checkBox = m_hmOptions.get(key);
			ConfigVariable variable = m_oPreferencesConfig.getVar(key);
			if(variable != null && checkBox != null)
				variable.setValue(checkBox.isSelected());
		}
		
		m_oConfMng.saveConfig();
		
		closeDlg();
	}
	
	/**
	 * Close the preferences dialog
	 */
	private void closeDlg()
	{
		dispose();
	}
}