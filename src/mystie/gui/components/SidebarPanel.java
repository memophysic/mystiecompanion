/**
 * Class for the sidebar of the application
 */

package mystie.gui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import mystie.gui.components.prjtree.ProjectTree;
import mystie.gui.components.prjtree.ProjectTreeCellRenderer;
import mystie.gui.components.prjtree.ProjectTreeDP;

import com.alee.extended.tree.WebAsyncTree;
import com.alee.laf.button.WebButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;

@SuppressWarnings({"serial"})
public class SidebarPanel extends WebPanel
{
	WebFrame m_oParentFrame;
	ProjectTree m_oPrjTree;
	
	public SidebarPanel(WebFrame parentFrame)
	{
		m_oParentFrame = parentFrame;
		setLayout(new BorderLayout());
	}
	
	/**
	 * Set the project root and create the tree content for it
	 * 
	 * @param root root directory as a file 
	 * @return true if a valid project was set as root, 
	 * false otherwise
	 */
	public boolean setProjectRoot(File root)
	{
		removeAll();
		if(root == null)
		{
			if(m_oPrjTree != null)
			{
				m_oPrjTree.removeAll();
				m_oPrjTree = null;
			}
			validate();
			repaint();
			return false;
		}
			
		final ProjectTreeDP treeDP = new ProjectTreeDP(root);
		m_oPrjTree = new ProjectTree(treeDP);
		styleTree(m_oPrjTree);

		WebScrollPane oTreeScroll = new WebScrollPane(m_oPrjTree);
		add(oTreeScroll, BorderLayout.CENTER);
		
		add(createSyncButton(m_oPrjTree), BorderLayout.SOUTH);
		
		validate();
		repaint();
		return true;
	}
	
	private static class SyncListener implements ActionListener
	{
		ProjectTree m_oTree;
		
		public SyncListener(ProjectTree oTree)
		{
			m_oTree = oTree;
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			m_oTree.reloadRootNode();
		}
	}
	
	private WebButton createSyncButton(ProjectTree oTree)
	{
		return new WebButton("Refresh", new SyncListener(oTree));
	}
	
	private void styleTree(ProjectTree oTree)
	{
		oTree.setCellRenderer(new ProjectTreeCellRenderer());
		oTree.setEditable(false);
		oTree.setSelectionMode(WebAsyncTree.SINGLE_TREE_SELECTION);
	}
}
