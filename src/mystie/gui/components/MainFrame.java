/**
 * Class for the main window frame of the application.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.alee.extended.panel.BorderPanel;
import com.alee.laf.rootpane.WebFrame;

import mystie.configuration.ConfigVariable;
import mystie.core.project.Project;
import mystie.core.project.ProjectConfig;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;
import mystie.gui.PresenterGui;
import mystie.gui.PresenterGuiConfig;
import mystie.gui.ResourcesManager;

@SuppressWarnings("serial")
public class MainFrame extends WebFrame
{
	public final String WINDOW_TITLE = "Mystie Companion";
	public final int FRAME_OFFSET = 25;
	
	private PresenterGui m_oParentPresenter;
	private MainMenubar m_oMenubar;
	private MainPanel m_oMainPanel;
	
	private boolean m_bFullscreen = false;
	Dimension m_dScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
	Rectangle m_oFrameRegion;
	
	/**
	 * Constructor for the main frame
	 * @param parentPresenter Presenter object to be linked to this frame
	 */
	public MainFrame(PresenterGui parentPresenter)
	{
		super();
		m_oParentPresenter = parentPresenter;
		
		setDefaultLookAndFeelDecorated(false);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setTitle(WINDOW_TITLE);
		setSize(800, 600);
		setShowResizeCorner(true);
		setIconImage(ResourcesManager.retrieveImage("icons/icon.png"));
		
		centerWindow();
		
		addWindowListener(new MainFrameAdapter(this));
		addComponentListener(new FrameComponentAdapter());
		
		// Components
		m_oMenubar = new MainMenubar(this);
		setJMenuBar(m_oMenubar);
		
		m_oMainPanel = new MainPanel(this);
		add(new BorderPanel(m_oMainPanel, 0));
	}
	
	/**
	 * Center the window's position on the screen
	 */
	public void centerWindow()
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2,
				    dim.height/2-this.getSize().height/2);
	}
	
	/**
	 * @return true if the frame is in fullscreen mode, false otherwise
	 */
	public boolean isFullscreen()
	{
		return m_bFullscreen;
	}
	
	public void setFullscreen(boolean bEnable)
	{
		if(bEnable == isFullscreen() || !isVisible())
			return;
		
		if(bEnable)
		{
			m_oFrameRegion = new Rectangle(
					getLocationOnScreen(),
					getSize());
			setBounds(0, 0,
					(int)m_dScreenSize.getWidth(),
					(int)m_dScreenSize.getHeight());
			/*setBounds(-FRAME_OFFSET, -FRAME_OFFSET,
					(int)m_dScreenSize.getWidth() + 2*FRAME_OFFSET,
					(int)m_dScreenSize.getHeight() + 2*FRAME_OFFSET);*/
		}
		else
		{
			if(m_oFrameRegion != null)
				setBounds(m_oFrameRegion);
		}
		
		m_bFullscreen = bEnable;
		boolean bBorderEnable = !m_bFullscreen;
		
		setShowTitleComponent(bBorderEnable);
		setShowWindowButtons(bBorderEnable);
		setResizable(bBorderEnable);
		setShowResizeCorner(bBorderEnable);
	}
	
	/**
	 * @return parent presenter to the frame
	 */
	public PresenterGui getParentPresenter()
	{
		return m_oParentPresenter;
	}
	
	/**
	 * Set the frame visible and setup the child components
	 */
	public void setVisibleAndSetup(boolean b)
	{
		super.setVisible(b);
		if(b)
			m_oMainPanel.initializeComponents();
	}
	
	/**
	 * Get the project currently loaded in workspace bound to same environment
	 * as the parent presentor's
	 * 
	 * @return project reference | null
	 */
	private Project getProject()
	{
		return getParentPresenter().getWorkspace().getProject();
	}
	
	public boolean loadProjectToInterface()
	{
		Project oPrj = getProject();
		
		boolean bSuccess = m_oMainPanel.loadProject(oPrj);
		if(bSuccess)
		{
			String sTitleSuffix = "";
			if(oPrj != null)
			{
				ProjectConfig oPrjCfg = oPrj.getConfig();
				sTitleSuffix = " - " + oPrjCfg.getTitle() + 
					" by " + oPrjCfg.getAuthor();
			}
			
			setTitle(WINDOW_TITLE + sTitleSuffix);
		}
		
		return bSuccess;
	}
	
	/**
	 * Ensures any processing or operation is completed before closing by 
	 * canceling or finish them.
	 */
	public void safeClose()
	{
		// TODO: Take out the generic closing mechanism (environment) from the
		// specific one
		try
		{
			PresenterGui pres = getParentPresenter();
			pres.saveAllConfigVar();
			pres.getConfigMng().saveConfig();
			
			Environment env = pres.getEnvironment();
			env.saveEnvironment();
			
			Workspace wspc = pres.getWorkspace();
			wspc.unloadProject(false);
			
			dispose();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			getParentPresenter().getHandler().alert(
					this,
					"Error while closing",
					"Cannot close safely, an error has occured:\n" +
							e.toString());
		}
	}
	
	/**
	 * Wrapper method for the project saving feature.
	 */
	public void saveProject()
	{
		Project project = getProject();
		if(project != null)
			project.saveProject();
	}
	
	/**
	 * Window adapter used by the main frame.
	 */
	private class MainFrameAdapter extends WindowAdapter
	{
		private WebFrame m_oParentFrame;
		
		public MainFrameAdapter(WebFrame parentFrame)
		{
			super();
			m_oParentFrame = parentFrame;
		}
		
		/**
		 * Check if the current project has modifications which needs 
		 * to be saved. If so, offer the user to save.
		 * @param e: Event of closing window
		 */
		@Override
		public void windowClosing(WindowEvent e)
		{
			Project oPrj = getProject();
			if(oPrj != null && oPrj.hasUnsavedChanges())
			{
				String[] options = {"Save and quit",
						"Quit anyways",
						"Cancel"};
				
				switch(m_oParentPresenter.getHandler().askThreeOptions(
						m_oParentFrame,
						"Unsaved project",
						"The current project has not been saved.\n" +
								"Would you like to save and quit?",
						options))
				{
					case 0:
						Environment env = getParentPresenter().getEnvironment();
						env.ACTS_PRJ.ACT_SAVE.execute(new Object[] {env});
						break;
					case 1:
						break;
					case 2:
					default:
						return;
				}
			}
			
			safeClose();
		}
	}
	
	private class FrameComponentAdapter extends ComponentAdapter
	{
		@Override
		public void componentResized(ComponentEvent e)
		{
			PresenterGui pres = getParentPresenter();
			assert(pres != null);
			
			int screenSize[] = {getWidth(), getHeight()};
			String keys[] = {PresenterGuiConfig.KEY_DEF_WIDTH,
							 PresenterGuiConfig.KEY_DEF_HEIGHT};
			for(int i = 0; i < 2; ++i)
			{
				ConfigVariable cv = pres.getConfigMng().getConfig().getVar(
						keys[i]);
				if(cv != null)
					cv.setValue(screenSize[i], false);
			}
		}
	}
}