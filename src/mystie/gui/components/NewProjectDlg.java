/**
 * New project dialog that allows the user to create a project parameters object
 * which will be used to construct a new project object
 */

package mystie.gui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;

import mystie.core.project.Project;
import mystie.core.project.ProjectConfig;
import mystie.ui.Handler;

import com.alee.extended.filechooser.WebDirectoryChooserPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.text.WebTextField;

@SuppressWarnings("serial")
public class NewProjectDlg extends WebDialog
{
	public final String WINDOW_TITLE = "New Project";
	
	private ProjectConfig m_oPrjParams;
	private WebButton m_oBtnOk, m_oBtnCancel;
	
	private Handler m_oHandler;
	
	private WebDirectoryChooserPanel m_oDirChsPnl;
	private HashMap<String, WebTextField> m_hmTextFields = 
			new HashMap<String, WebTextField>();
	
	public NewProjectDlg(Component parent, Handler handler)
	{
		super(parent);
		m_oHandler = handler;
		
		setupDialog(parent);
	}
	
	/**
	 * Setup dialog properties
	 * 
	 * @param parent parent component
	 */
	private void setupDialog(Component parent)
	{
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(true);
		setShowResizeCorner(true);
		setSize(600, 500);
		setLocationRelativeTo(parent);
		setTitle(WINDOW_TITLE);
		
		WebPanel contentPanel = new WebPanel();
		contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		add(contentPanel);
		
		contentPanel.add(createFieldsPanel(), BorderLayout.CENTER);
		contentPanel.add(createButtonsPanel(), BorderLayout.SOUTH);
	}
	
	private WebPanel createFieldsPanel()
	{
		WebPanel oPanel = new WebPanel();
		oPanel.setLayout(
				new BoxLayout(oPanel, BoxLayout.Y_AXIS));
		
		WebPanel oPrjDirPnl = new WebPanel();
		oPrjDirPnl.setLayout(new FlowLayout(FlowLayout.LEFT));
		oPanel.add(oPrjDirPnl);
		
		WebLabel oDirLbl = new WebLabel("Project Directory");
		oPrjDirPnl.add(oDirLbl);
		
		m_oDirChsPnl = new WebDirectoryChooserPanel();
		oPanel.add(m_oDirChsPnl);
		
		m_hmTextFields.put(ProjectConfig.KEY_TITLE,
				addTextField(oPanel,
				"Project Title: ", "Enter project title..."));
		m_hmTextFields.put(ProjectConfig.KEY_AUTHOR,
				addTextField(oPanel,
				"Author: ", "User name..."));
		
		return oPanel;
	}
	
	/**
	 * Add a text field to the passed panel
	 * 
	 * @param oPanel panel to which the text field is added
	 * @param fieldTitle field title
	 * @param fieldText field default text
	 * @return field object added to the panel
	 */
	private WebTextField addTextField(WebPanel oPanel, String fieldTitle, 
			String fieldText)
	{
		WebPanel oNewPanel = new WebPanel();
		oNewPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		oPanel.add(oNewPanel);
		
		WebLabel oPrjTtlLbl = new WebLabel(fieldTitle);
		oPrjTtlLbl.setPreferredWidth(100);
		oNewPanel.add(oPrjTtlLbl);
		
		WebTextField oField = new WebTextField(true);
		oField.setPreferredWidth(200);
		oField.setInputPrompt(fieldText);
		oNewPanel.add(oField);
		
		return oField;
	}
	
	
	/**
	 * Create button panel
	 */
	private WebPanel createButtonsPanel()
	{
		WebPanel oBtnPanel = new WebPanel();
		oBtnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		m_oBtnOk = new WebButton("Create Project");
		m_oBtnOk.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		    	createProjectParameters();
		    }});
		m_oBtnOk.setSize(90, 30);
		//m_oBtnOk.setEnabled(false);
		m_oBtnOk.setBottomBgColor(new Color(130,170,240));
		oBtnPanel.add(m_oBtnOk);
		
		m_oBtnCancel = new WebButton("Cancel");
		m_oBtnCancel.setSize(90, 30);
		m_oBtnCancel.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		    	dispose();
		    }});
		oBtnPanel.add(m_oBtnCancel);
		
		return oBtnPanel;
	}
	
	/**
	 * @return true if the fields' values are valid, false otherwise
	 */
	private boolean isProjectParamValid()
	{
		boolean valid = true;
		
		File oPrjDir = m_oDirChsPnl.getSelectedDirectory();
		valid &= oPrjDir != null && 
				oPrjDir.exists() && 
				oPrjDir.isDirectory();
		
		String sTitle = m_hmTextFields.get(
				ProjectConfig.KEY_TITLE).getText();
		valid &= sTitle != null && !sTitle.isEmpty();
		
		String sAuthor = m_hmTextFields.get(
				ProjectConfig.KEY_AUTHOR).getText();
		valid &= sAuthor != null && !sAuthor.isEmpty();
		
		return valid;
	}
	
	/**
	 * Create the project parameters object using the fields filed by the user
	 * in the dialog
	 */
	private void createProjectParameters()
	{
		if(isProjectParamValid())
		{
			m_oPrjParams = new ProjectConfig(
				m_hmTextFields.get(ProjectConfig.KEY_TITLE).getText(),
				m_hmTextFields.get(ProjectConfig.KEY_AUTHOR).getText(),
				new File(m_oDirChsPnl.getSelectedDirectory().getPath() + 
						 File.separator + Project.FILENAME),
				0);
			dispose();
		}
		else
		{
			m_oHandler.alert(this, "New Project Error",
					"Cannot create project, some parameters are invalid");
		}
	}
	
	/**
	 * @return project parameter object supposed to be created by the dialog. 
	 * 		   can be null if the dialog was cancelled
	 */
	public ProjectConfig getValue()
	{
		return m_oPrjParams;
	}
	
	/**
	 * Open the dialog as a modal dialog, returning the value from getValue
	 * when disposed
	 * @return ProjectParameters object from getValue method
	 */
	public ProjectConfig doModal()
	{
		setVisible(true);
		return getValue();
	}
}