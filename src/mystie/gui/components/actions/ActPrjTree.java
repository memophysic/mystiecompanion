package mystie.gui.components.actions;

import java.awt.Image;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import com.alee.extended.tree.AsyncUniqueNode;
import com.alee.extended.tree.WebAsyncTree;

import mystie.gui.ResourcesManager;
import mystie.gui.components.menu.TreeMenuItem;

public class ActPrjTree
{
	@SuppressWarnings("serial")
	public static class RefreshTreeMenuItem<E extends AsyncUniqueNode> 
		extends TreeMenuItem<E>
	{
		public static final Image img = ResourcesManager.retrieveImage(
				"icons/refresh.png", ICON_SIZE_DEFAULT);
		public static final ImageIcon imgIcn = img != null ?
				new ImageIcon(img) : null;
		
		public RefreshTreeMenuItem(WebAsyncTree<E> oTree, E oNode)
		{
			super(oTree, oNode, "Refresh", imgIcn);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			getTree().reloadNode(getNode());
		}
	}
}
