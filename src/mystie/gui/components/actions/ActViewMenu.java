/**
 * Action Listeners for the view menu
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components.actions;

import java.awt.event.ActionEvent;

import mystie.configuration.ConfigManager;
import mystie.configuration.ConfigVariable;
import mystie.gui.PresenterGui;
import mystie.gui.PresenterGuiConfig;
import mystie.gui.components.MainFrame;
import mystie.gui.components.menu.FrameMenuItem;

import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.hotkey.HotkeyData;

public class ActViewMenu
{
	@SuppressWarnings("serial")
	public static class Fullscreen extends FrameMenuItem
	{
		public Fullscreen(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			WebFrame oFrame = getParentFrame();
			if(oFrame instanceof MainFrame)
			{
				MainFrame oMainFrame = (MainFrame)oFrame;
				
				PresenterGui pres = oMainFrame.getParentPresenter();
				assert(pres != null);
				
				ConfigManager oCfgMng = pres.getConfigMng();
				ConfigVariable cv = oCfgMng.getConfig().getVar(
						PresenterGuiConfig.KEY_FULLSCREEN);
				if(cv != null)
					cv.setValue(!oMainFrame.isFullscreen());
			}
		}
	}
}