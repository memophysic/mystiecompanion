/**
 * Action Listeners for the edit menu
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components.actions;

import java.awt.event.ActionEvent;

import mystie.gui.components.menu.FrameMenuItem;

import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.hotkey.HotkeyData;

public class ActEditMenu
{
	@SuppressWarnings("serial")
	public static class Cut extends FrameMenuItem
	{
		public Cut(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public boolean getConditionalEnable()
		{
			return false;
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("Cut");
		}
	}
	
	@SuppressWarnings("serial")
	public static class Copy extends FrameMenuItem
	{
		public Copy(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public boolean getConditionalEnable()
		{
			return false;
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("Copy");
		}
	}
	
	@SuppressWarnings("serial")
	public static class Paste extends FrameMenuItem
	{
		public Paste(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public boolean getConditionalEnable()
		{
			return false;
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("Paste");
		}
	}
}