/**
 * Action Listeners for the file menu
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui.components.actions;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.File;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.laf.rootpane.WebFrame;
import com.alee.managers.hotkey.HotkeyData;
import com.alee.utils.swing.DialogOptions;

import mystie.configuration.ConfigManager;
import mystie.core.project.ProjectConfig;
import mystie.environment.Environment;
import mystie.environment.EnvironmentManager;
import mystie.gui.components.MainFrame;
import mystie.gui.components.PreferencesDlg;
import mystie.gui.components.menu.FrameMenuItem;

public class ActFileMenu
{
	@SuppressWarnings("serial")
	public static class NewProject extends FrameMenuItem
	{
		public NewProject(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			MainFrame oFrame = (MainFrame) getParentFrame();
			assert(oFrame != null);
			
			ProjectConfig prjParams = env.getHandler().createProjectConfig(
					oFrame);
			
			if (prjParams == null)
				return;
			
			if(0 == env.ACTS_PRJ.ACT_CREATE_AND_LOAD.execute(
						new Object[] {prjParams, env}))
				oFrame.loadProjectToInterface();
		}
	}
	
	@SuppressWarnings("serial")
	public static class OpenProject extends FrameMenuItem
	{
		public OpenProject(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}
		
		@Override
		public void actionPerformed(ActionEvent e)
		{
			final Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			final MainFrame oFrame = (MainFrame) getParentFrame();
			assert(oFrame != null);
			
			File oPrjDir = null;
			WebDirectoryChooser dirChs = new WebDirectoryChooser(
					oFrame, "Choose the project directory");
			dirChs.setVisible(true);
			
            if(dirChs.getResult() == DialogOptions.OK_OPTION)
            {
            	oPrjDir = dirChs.getSelectedDirectory();
				if(0 == env.ACTS_PRJ.ACT_OPEN.execute(
							new Object[] {env, oPrjDir}))
					oFrame.loadProjectToInterface();
				else
				{
					env.getHandler().alert(
						oFrame, "Open File",
						"No valid project found in the specified directory.");
				}
            }
		}
	}
	
	@SuppressWarnings("serial")
	public static class SaveProject extends FrameMenuItem
	{
		public SaveProject(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}

		@Override
		public boolean getConditionalEnable()
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			return env.getWorkspace().getProject() != null;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			env.ACTS_PRJ.ACT_SAVE.execute(new Object[] {env});
		}
	}
	
	@SuppressWarnings("serial")
	public static class CloseProject extends FrameMenuItem
	{
		public CloseProject(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}

		@Override
		public boolean getConditionalEnable()
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			return env.getWorkspace().getProject() != null;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			assert(env != null);
			
			if(env.ACTS_PRJ.ACT_CLOSE.execute(new Object[] {env}) == 0)
			{
				MainFrame oFrame = (MainFrame) getParentFrame();
				assert(oFrame != null);
				oFrame.loadProjectToInterface();
			}
		}
	}
	
	@SuppressWarnings("serial")
	public static class Preferences extends FrameMenuItem
	{
		public Preferences(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Environment env = EnvironmentManager.getActiveEnvironment();
			if (env != null)
			{
				ConfigManager oConfigMng = env.getPresenter().getConfigMng();
				if (oConfigMng != null)
				{
					PreferencesDlg oPrefDlg = new PreferencesDlg(
							getParentFrame(), oConfigMng);
					oPrefDlg.setVisible(true);
				}
			}
		}
	}
	
	@SuppressWarnings("serial")
	public static class Quit extends FrameMenuItem
	{
		public Quit(WebFrame oMainFrame,
				String sTitle, String sTooltip, HotkeyData hotkey)
		{
			super(oMainFrame, sTitle, sTooltip, hotkey);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			WebFrame parentFrame = getParentFrame();
			if (parentFrame != null)
				parentFrame.dispatchEvent(
						new WindowEvent(parentFrame,
										WindowEvent.WINDOW_CLOSING));
		}
	}
}
