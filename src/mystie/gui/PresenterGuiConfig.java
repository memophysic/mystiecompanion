/**
 * Configuration for the application
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui;

import mystie.configuration.BaseConfiguration;
import mystie.configuration.ConfigVariable;

public class PresenterGuiConfig extends BaseConfiguration
{
	public static final String KEY_DEF_WIDTH = "MC_KEY_DEFW";
	public static final Integer DVL_DEF_WIDTH = 800;
	public static final String LBL_DEF_WIDTH = "Window Width";
	
	public static final String KEY_DEF_HEIGHT = "MC_KEY_DEFH";
	public static final Integer DVL_DEF_HEIGHT = 600;
	public static final String LBL_DEF_HEIGHT = "Window Height";
	
	public static final String KEY_FULLSCREEN = "ENVCFG_FLSCRN";
	public static final Boolean DVL_FULLSCREEN = false;
	public static final String LBL_FULLSCREEN = "Fullscreen";
	
	public static final String KEY_ALWAYS_TOP = "MC_KEY_ALWAYS_TOP";
	public static final Boolean DVL_ALWAYS_TOP = false;
	public static final String LBL_ALWAYS_TOP = "Keep window on top";
	
	public static final String[] FIELD_KEYS = 
		{KEY_DEF_WIDTH, KEY_DEF_HEIGHT, KEY_FULLSCREEN, KEY_ALWAYS_TOP};
	
	public PresenterGuiConfig()
	{
		m_hmConfigVar.put(KEY_DEF_WIDTH, new ConfigVariable(
				KEY_DEF_WIDTH, Integer.class, 
				DVL_DEF_WIDTH, LBL_DEF_WIDTH));
		
		m_hmConfigVar.put(KEY_DEF_HEIGHT, new ConfigVariable(
				KEY_DEF_HEIGHT, Integer.class, 
				DVL_DEF_HEIGHT, LBL_DEF_HEIGHT));
		
		m_hmConfigVar.put(KEY_FULLSCREEN, new ConfigVariable(
				KEY_FULLSCREEN, Boolean.class,
				DVL_FULLSCREEN, LBL_FULLSCREEN));
		
		m_hmConfigVar.put(KEY_ALWAYS_TOP, new ConfigVariable(
				KEY_ALWAYS_TOP, Boolean.class, 
				DVL_ALWAYS_TOP, LBL_ALWAYS_TOP));
	}
}
