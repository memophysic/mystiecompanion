/**
 * Implementation of the base handler as a GUI handler which will use graphical
 * tools and implemented graphical instances to gather the necessary 
 * information.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui;

import java.awt.Component;
import javax.swing.JOptionPane;

import mystie.core.project.ProjectConfig;
import mystie.gui.components.NewProjectDlg;
import mystie.ui.BaseHandler;

public class HandlerGui extends BaseHandler
{
	/**
	 * Prompt message box with yes/no options
	 */
	@Override
	public boolean askYesNo(Object oCaller, String sCallerDesc, String sMsg)
	{
		return JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(
				getCastedComponent(oCaller),
				sMsg,
				sCallerDesc,
				JOptionPane.YES_NO_OPTION);		 
	}
	
	/**
	 * Prompt message box with the options passed
	 */
	@Override
	public int askThreeOptions(Object oCaller, String sCallerDesc, String sMsg,
			String[] options)
	{
		int nOptChosen = JOptionPane.showOptionDialog(
			getCastedComponent(oCaller),
			sMsg,
			sCallerDesc,
			JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.WARNING_MESSAGE,
			null,
			options,
			options[2]);
		
		switch(nOptChosen)
		{
			default:
			case JOptionPane.YES_OPTION:
				return 0;
			case JOptionPane.NO_OPTION:
				return 1;
			case JOptionPane.CANCEL_OPTION:
				return 2;
		}
	}
	
	/**
	 * Show a message box with an error sign
	 */
	@Override
	public void alert(Object oCaller, String sCallerDesc, String sMsg)
	{
		showMsgDlg(getCastedComponent(oCaller), sCallerDesc, sMsg,
				JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Show a message box with an exclamation sign
	 */
	@Override
	public void inform(Object oCaller, String sCallerDesc, String sMsg)
	{
		showMsgDlg(getCastedComponent(oCaller), sCallerDesc, sMsg,
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Show message box with 'Ok' button indicating the message as a label
	 * and the caller description in the title.
	 */
	private void showMsgDlg(Component parent, String sTitle, String sMsg,
			int nType)
	{
		JOptionPane.showMessageDialog(parent, sMsg, sTitle, nType);
	}
	
	/**
	 * @param obj Any object
	 * @return component-casted object if it is of instance, null otherwise
	 */
	private Component getCastedComponent(Object obj)
	{
		try
		{
			if(obj instanceof Component)
				return Component.class.cast(obj);
			else
				return null;
		}
		catch(ClassCastException e)
		{
			return null;
		}
	}
	
	/**
	 * Open new project dialog to select the values of the parameters
	 * 
	 * @param oCaller caller object which called the handler
	 */
	@Override
	public ProjectConfig createProjectConfig(Object oCaller)
	{
		NewProjectDlg newPrjDlg = new NewProjectDlg(
				getCastedComponent(oCaller), this);
		return newPrjDlg.doModal();
	}
}
