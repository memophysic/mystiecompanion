/**
 * Class for the application's splash screen
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class SplashScreen implements Runnable
{
	JWindow frame;
	JPanel translucentPane;
	
	@Override
	public void run()
	{
		translucentPane = new JPanel();
		translucentPane.setOpaque(false);
		
		frame = new JWindow();
        frame.setAlwaysOnTop(true);
        frame.setBackground(new Color(0,0,0,0));
        frame.setContentPane(translucentPane);
        frame.add(new JLabel(new ImageIcon(
        		ResourcesManager.retrieveImage("splash.png"))));
        frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public void show()
	{
		try
		{
			EventQueue.invokeAndWait(this);
		}
		catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	public void hide()
	{
		if(frame != null)
		{
			frame.dispose();
			frame = null;
			translucentPane = null;
		}
	}
}
