/**
 * Implementation of the presenter for the gui, which holds the main frame
 * and other components of the gui.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui;

import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;

import javax.swing.SwingUtilities;

import mystie.BasePresenter;
import mystie.configuration.ConfigManager;
import mystie.configuration.ConfigVariable;
import mystie.configuration.Configuration;
import mystie.core.FileTools;
import mystie.environment.Environment;
import mystie.gui.PresenterGuiConfig;
import mystie.gui.components.MainFrame;

public class PresenterGui extends BasePresenter
{
	private SplashScreen m_oSplashScreen;
	private MainFrame m_oFrame;
	
	public final String PRESGUI_DEF_CONFIG_PATH = 
		FileTools.getMystieHomePath() + File.separator + "presGuiConfig.xml";
	
	public PresenterGui(Environment env)
	{
		super(env, new HandlerGui(), new AppSetupGui());
		
		ConfigManager oConfigMng = getConfigMng();
		oConfigMng.initializeConfig(PresenterGuiConfig.class,
									PRESGUI_DEF_CONFIG_PATH);
		oConfigMng.getConfig().observeAll(this);
	}
	
	@Override
	public void initialize()
	{
		final PresenterGui parent = this;
		
		try
		{
			SwingUtilities.invokeAndWait(new Runnable() {
				public void run()
				{
					getAppSetup().setupAll();
					
					m_oFrame = new MainFrame(parent);
					m_oFrame.setVisible(true);
					
					applyAllConfig();
					
					m_oFrame.setVisibleAndSetup(true);
				}
			});
		}
		catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Shows a splash screen for the starting application if start is true,
	 * hides it if start is false.
	 */
	@Override
	public void presentInitializationLoading(boolean start)
	{
		if(start)
		{
			if(m_oSplashScreen != null)
				return;
			
			m_oSplashScreen = new SplashScreen();
			m_oSplashScreen.show();
		}
		else
		{
			if(m_oSplashScreen == null)
				return;
			
			m_oSplashScreen.hide();
			m_oSplashScreen = null;
		}
	}
	
	public void saveAllConfigVar()
	{
		
		Configuration oConfig = getConfigMng().getConfig();
		for(String key : oConfig.getKeys())
		{
			ConfigVariable cv = oConfig.getVar(key);
			switch(cv.getKeyCode())
			{
				case PresenterGuiConfig.KEY_DEF_WIDTH:
				{
					cv.setValue(m_oFrame.getWidth(), false);
					break;
				}
				case PresenterGuiConfig.KEY_DEF_HEIGHT:
				{
					cv.setValue(m_oFrame.getHeight(), false);
					break;
				}
				default:
					break;
			}
		}
	}

	@Override
	public void update(Observable o, Object arg)
	{
		if(o instanceof ConfigVariable)
			applyConfigVar((ConfigVariable)o);
	}
	
	public void applyConfigVar(ConfigVariable cv) 
	{
		switch(cv.getKeyCode())
		{
			case PresenterGuiConfig.KEY_DEF_WIDTH:
			{
				Dimension dim = m_oFrame.getSize();
				m_oFrame.setSize((int)cv.getValue(), dim.height);
				break;
			}
			case PresenterGuiConfig.KEY_DEF_HEIGHT:
			{
				Dimension dim = m_oFrame.getSize();
				m_oFrame.setSize(dim.width, (int)cv.getValue());
				break;
			}
			case PresenterGuiConfig.KEY_FULLSCREEN:
			{
				m_oFrame.setFullscreen((boolean)cv.getValue());
				break;
			}
			case PresenterGuiConfig.KEY_ALWAYS_TOP:
			{
				m_oFrame.setAlwaysOnTop((boolean)cv.getValue());
				break;
			}
			default:
				break;
		}
	}
	
	public void applyAllConfig()
	{
		Configuration oConfig = getConfigMng().getConfig();
		for(String key : oConfig.getKeys())
			applyConfigVar(oConfig.getVar(key));
	}

	@Override
	public void presentProject()
	{
		assert(m_oFrame != null);
		m_oFrame.loadProjectToInterface();
	}
}
