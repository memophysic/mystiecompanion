package mystie.gui;

import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.util.HashMap;

public class ResourcesManager
{
	private static HashMap<String, Image> m_hmImageCache = 
			new HashMap<String, Image>();
	
	/**
	 * Retrieve an image from the img directory
	 * @param resFilename filename of the image
	 * @return the image if found, null otherwise
	 */
	public static Image retrieveImage(String resFilename)
	{
		Image img = m_hmImageCache.get(resFilename);
		if(img != null)
			return img;
		
		String fullFilename = "/img/" + resFilename;
		URL url = ResourcesManager.class.getResource(fullFilename);
		if(url != null) {
			Toolkit kit = Toolkit.getDefaultToolkit();
			img = kit.createImage(url);
			if(img != null)
				m_hmImageCache.put(resFilename, img);
		}
		return img;
	}
	
	/**
	 * Retrieve a scaled image from the resources
	 * @param resFilename filename of the image
	 * @param size width and height to target scaled image
	 * @return the scaled buffered image if found, null otherwise
	 */
	public static Image retrieveImage(String resFilename, int size)
	{
		return retrieveImage(resFilename, size, size);
	}
	
	/**
	 * Retrieve a scaled image from the resources
	 * @param resFilename filename of the image
	 * @param width width to target scaled image
	 * @param height height to target scaled image
	 * @return the scaled buffered image if found, null otherwise
	 */
	public static Image retrieveImage(String resFilename,
			int width, int height)
	{
		Image img = retrieveImage(resFilename);
		if(img != null)
			return resizeImage(img, width, height);
		return img;
	}
	
	/**
	 * Resize an image to the passed size
	 * @param img image to scale
	 * @param width width to target scaled image
	 * @param height height to target scaled image
	 * @return Appropriately scaled BufferedImage
	 */
	public static Image resizeImage(Image img, int width, int height)
	{
		return img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}
	
	/*public static Image resizeImageCustom(Image img, int width, int height)
	{
		BufferedImage bi = new BufferedImage(width, height,
		BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = bi.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
							RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
							RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.drawImage(img, 0, 0, width, height, null);
		g.dispose();
		
		return bi;
	}*/
}
