/**
 * Setup functions for the application which allows to set different parameters
 * for the GUI handled by Java classes.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.gui;

import java.awt.Font;
import java.util.Locale;
import javax.swing.JFileChooser;

import com.alee.laf.WebLookAndFeel;

import mystie.BaseAppSetup;

public class AppSetupGui extends BaseAppSetup
{
	/**
	 * Wrapper method to setup the whole GUI
	 */
	public void setupAll()
	{
		setupLanguage();
		setupLAF();
	}
	
	/**
	 * Setup the default language for the implemented messages
	 */
	private void setupLanguage()
	{
		JFileChooser.setDefaultLocale(Locale.ENGLISH);
	}
	
	/**
	 * Setup the look and feel (LAF) of the application
	 */
	private void setupLAF()
	{
    	setWeblafFonts(new Font("Tahoma", Font.PLAIN, 12));
    	WebLookAndFeel.setDecorateAllWindows(true);
        WebLookAndFeel.install();
	}
	
	/**
	 * Assign the passed font to all WebLaf's components
	 */
	private void setWeblafFonts(Font newFont)
	{
		if(newFont == null)
			return;
		
		WebLookAndFeel.globalTextFont = newFont;
		WebLookAndFeel.globalAcceleratorFont = newFont;
		WebLookAndFeel.globalTitleFont = newFont;
		WebLookAndFeel.globalMenuFont = newFont;
		WebLookAndFeel.globalAlertFont = newFont;
		WebLookAndFeel.globalControlFont = newFont;
	}
}
