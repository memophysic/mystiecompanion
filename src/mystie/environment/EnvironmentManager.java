/**
 * Application main which handles the setup of the interface and loading
 * of configuration files.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.environment;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import mystie.core.workspace.BaseWorkspace;
import mystie.environment.Environment;
import mystie.exceptions.NoSuchEnvironmentException;
import mystie.gui.PresenterGui;

public class EnvironmentManager
{
	public static final Class<PresenterGui> DEFAULT_PRESENTATOR = 
			PresenterGui.class;
	public static final Class<BaseWorkspace> DEFAULT_WORKSPACE = 
			BaseWorkspace.class;
	
	private static HashMap<UUID, Environment> m_hmEnvironment = 
			new HashMap<UUID, Environment>();
	private static UUID m_activeEnvironmentKey;
	
	/**
	 * Create the default environment for the application and add it to the 
	 * environment manager
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static void createDefaultEnvironment()
			throws InstantiationException, IllegalAccessException
	{
		try
		{
			Environment env = new Environment(
					DEFAULT_WORKSPACE,
					DEFAULT_PRESENTATOR);
			
			addEnvironment(env);
		}
		catch (IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Add an environment to the vector
	 * 
	 * @param env environment object to add
	 * @throws NullPointerException
	 */
	public static void addEnvironment(Environment env)
	{
		if(env != null)
		{
			boolean setNewSelection = m_hmEnvironment.size() == 0;
			m_hmEnvironment.put(env.getKey(), env);
			
			if(setNewSelection)
				setActiveEnvironment(env);
		}
		else
			throw new NullPointerException("Cannot add environment: " +
					"It is null");
	}
	
	/**
	 * Remove the passed environment from the registered environments.<br>
	 * Chooses another active environment if the removed one was active.
	 * 
	 * @param env environment object to remove
	 */
	public static void removeEnvironment(Environment env)
			throws NoSuchEnvironmentException
	{
		removeEnvironment(env.getKey());
	}
	
	/**
	 * Remove the entry linked to the passed environment key from the 
	 * registered environments.<br>
	 * Chooses another active environment if the removed one was active.
	 * 
	 * @param envKey key to the environment to be removed
	 */
	public static void removeEnvironment(UUID envKey)
			throws NoSuchEnvironmentException
	{
		Environment activeEnv = getActiveEnvironment();
		Environment env = m_hmEnvironment.remove(envKey);
		if(env == activeEnv)
		{
			Collection<Environment> envs = m_hmEnvironment.values();
			if(!envs.isEmpty())
				setActiveEnvironment(envs.iterator().next());
		}
	}
	
	/**
	 * Match the environment object desired with the passed key
	 * 
	 * @param key Object acting as a key
	 * @return environment object corresponding to the key, null if not found
	 */
	public static Environment getEnvironment(Object key)
	{
		return m_hmEnvironment.get(key);
	}
	
	/**
	 * Set the currently active environment
	 * @param env Environment object reference
	 */
	public static void setActiveEnvironment(Environment env)
	{
		setActiveEnvironment(env.getKey());
	}
	
	/**
	 * Set the currently active environment
	 * @param envKey key to the environment
	 */
	public static void setActiveEnvironment(UUID envKey)
	{
		m_activeEnvironmentKey = envKey;
	}
	
	public static void resetActiveEnvironment()
	{
		m_activeEnvironmentKey = null;
	}
	
	/**
	 * Get the currently active environment
	 * @return Environment object reference
	 */
	public static Environment getActiveEnvironment()
	{
		return m_hmEnvironment.get(m_activeEnvironmentKey);
	}
	
	/**
	 * Clear the environment vector
	 */
	public static void clearEnvironment()
	{
		m_hmEnvironment.clear();
		resetActiveEnvironment();
	}
}