/**
 * Contains the whole application's object data and act as a holder for the 
 * different module instances.
 * <br>
 * Because each module of the application can be of different types (console, 
 * gui, etc.), the environment is provided all the different implementations
 * of the modules and holds them for a session. Through the same process, it 
 * is therefore possible to run multiple environments.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.environment;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.Vector;

import mystie.Module;
import mystie.Presenter;
import mystie.configuration.ConfigManager;
import mystie.core.FileTools;
import mystie.core.project.actions.PrjActions;
import mystie.core.workspace.Workspace;
import mystie.ui.Handler;

public class Environment
{
	private static final String CONFIG_FILENAME = "envConfig.xml";
	private static final String CONFIG_PATH = 
			FileTools.getMystieHomePath() + File.separator + CONFIG_FILENAME;
	
	private ConfigManager m_oConfMgn;
	private Vector<Module> m_vModules;
	private Workspace m_oWorkspace;
	private Presenter m_oPresenter;
	private UUID m_oKey = UUID.randomUUID();
	
	// Core actions
	public final PrjActions ACTS_PRJ = new PrjActions();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Environment(Class wkspcCls, Class presCls) 
		throws InstantiationException, IllegalAccessException,
			   IllegalArgumentException, InvocationTargetException,
			   NoSuchMethodException, SecurityException
	{
		m_oConfMgn = new ConfigManager();
		m_oConfMgn.initializeConfig(EnvironmentConfig.class,
									CONFIG_PATH);
		
		// Create modules and register them
		m_vModules = new Vector<Module>();
		
		m_oWorkspace = (Workspace) wkspcCls.
				getConstructor(Environment.class).newInstance(this);
		assert(m_oWorkspace != null);
		m_vModules.add(m_oWorkspace);
		
		m_oPresenter = (Presenter) presCls.
				getConstructor(Environment.class).newInstance(this);
		assert(m_oPresenter != null);
		m_vModules.add(m_oPresenter);
		
		// Initialize the whole application
		m_oPresenter.presentInitializationLoading(true);
		initializeModules();
		loadLastProject();
		m_oPresenter.presentInitializationLoading(false);
	}
	
	/**
	 * @return the key object used to retrieve an environment object
	 */
	public UUID getKey()
	{
		return m_oKey;
	}
	
	/**
	 * @return workspace object reference
	 */
	public Workspace getWorkspace()
	{
		return m_oWorkspace;
	}
	
	/**
	 * @return presenter object reference
	 */
	public Presenter getPresenter()
	{
		return m_oPresenter;
	}
	
	/**
	 * @return returns the handler used to interact with user
	 */
	public Handler getHandler()
	{
		assert(m_oPresenter != null);
		return m_oPresenter.getHandler();
	}
	
	/**
	 * Initialize all the modules contained in the environment
	 */
	private void initializeModules()
	{
		for(Module mod : m_vModules)
			mod.initialize();
	}
	
	public File getLastProjectDirectory()
	{
		EnvironmentConfig cfg = (EnvironmentConfig)m_oConfMgn.getConfig();
		return cfg.getLastProject();
	}
	
	/**
	 * Load the last project loaded
	 */
	public void loadLastProject()
	{
		File oPrjDir = getLastProjectDirectory();
		if(oPrjDir != null && oPrjDir.isDirectory())
			ACTS_PRJ.ACT_OPEN.execute(new Object[] {this, oPrjDir});
	}
	
	public void setLastProjectLoaded(File oPrjDir)
	{
		EnvironmentConfig cfg = (EnvironmentConfig)m_oConfMgn.getConfig();
		cfg.setLastProject(oPrjDir);
	}
	
	public boolean saveEnvironment()
	{
		return m_oConfMgn.saveConfig();
	}
}
