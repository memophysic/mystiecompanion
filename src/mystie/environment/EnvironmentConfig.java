/**
 * Specific configuration for an environment
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.environment;

import java.io.File;

import mystie.configuration.BaseConfiguration;
import mystie.configuration.ConfigVariable;

public class EnvironmentConfig extends BaseConfiguration
{
	public static final String KEY_FIRST_TIME_USE = "ENVCFG_FST_TM_USE";
	public static final Boolean DVL_FIRST_TIME_USE = true;
	public static final String LBL_FIRST_TIME_USE = "First time use";
	
	public static final String KEY_LAST_PROJECT = "ENVCFG_LAST_PRJ";
	public static final File DVL_LAST_PROJECT = null;
	public static final String LBL_LAST_PROJECT = "Last Project Opened";
	
	public static final String[] FIELD_KEYS = 
		{KEY_FIRST_TIME_USE, KEY_LAST_PROJECT};
	
	public EnvironmentConfig()
	{
		m_hmConfigVar.put(KEY_FIRST_TIME_USE, new ConfigVariable(
			KEY_FIRST_TIME_USE, Boolean.class,
			DVL_FIRST_TIME_USE, LBL_FIRST_TIME_USE));
		
		m_hmConfigVar.put(KEY_LAST_PROJECT, new ConfigVariable(
				KEY_LAST_PROJECT, File.class,
				DVL_LAST_PROJECT, LBL_LAST_PROJECT));
	}
	
	public File getLastProject()
	{
		return (File) m_hmConfigVar.get(KEY_LAST_PROJECT).getValue();	
	}
	
	public void setLastProject(File oProjectDir)
	{
		m_hmConfigVar.get(KEY_LAST_PROJECT).setValue(oProjectDir);
	}
}
