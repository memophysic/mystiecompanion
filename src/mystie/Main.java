/**
 * Main for the application
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie;

import mystie.environment.EnvironmentManager;

public class Main
{
	public static void initiate()
	{
		try
		{
			EnvironmentManager.createDefaultEnvironment();
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		initiate();
	}
}
