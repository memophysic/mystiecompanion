/**
 * Class interfacing the handling of configuration file with an appropriate 
 * delegate implementing the configuration interface.
 * <br><br>
 * The class extending this class must call setConfigClass to set its own 
 * configuration implementation.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Observable;

import com.thoughtworks.xstream.XStream;

@SuppressWarnings("rawtypes")
public class ConfigManager
{
	private Class m_cConfigCls;
	private XStream m_oXStream;
	private Configuration m_oConfig;
	
	public ConfigManager()
	{
		m_oXStream = new XStream();
		m_oXStream.omitField(Observable.class, "obs");
		m_oXStream.omitField(Observable.class, "changed");
		m_oXStream.omitField(BaseConfiguration.class, "m_oSaveFile");
	}
	
	/**
	 * Wrapper method allowing to set the configuration class-delegate and 
	 * load the default path or create a new configuration object if no file
	 * is found
	 * 
	 * @param cls class-delegate
	 * @param sPath path to a xml file containing the data of the config file
	 */
	public boolean initializeConfig(Class cls, String sPath)
	{
		setConfigClass(cls);
		return loadOrCreateConfig(sPath);
	}
	
	/**
	 * Set the already created configuration instance as current configuration
	 * and try to save the file. If it fails, the configuration is returned to
	 * its original value.
	 * 
	 * @param oConfig Configuration instance to set
	 * @param sPath path where to save the configuration
	 * @return true if success to save, false otherwise
	 */
	public boolean setAndSaveConfig(Configuration oConfig, String sPath)
	{
		Configuration prevCfg = getConfig();
		setConfig(oConfig);
		
		boolean bSuccess = saveConfig(sPath);
		if(!bSuccess)
			setConfig(prevCfg);
		return bSuccess;
	}
	
	/**
	 * Get XStream object for this configuration manager
	 * 
	 * @return XStream instance
	 */
	private XStream getXStream()
	{
		return m_oXStream;
	}
	
	/**
	 * Set the configuration class-delegate to be used in de-serialization
	 * 
	 * @param cls class-delegate
	 */
	public void setConfigClass(Class cls)
	{
		m_cConfigCls = cls;
	}
	
	/**
	 * Return the currently set class-delegate
	 * 
	 * @return class-delegate
	 */
	public Class getConfigClass()
	{
		return m_cConfigCls;
	}
	
	/**
	 * Return the current configuration instance
	 * 
	 * @return configuration instance currently loaded
	 */
	public Configuration getConfig()
	{
		return m_oConfig;
	}
	
	/**
	 * Set the configuration instance as current configuration
	 * 
	 * @param oConfig
	 */
	public void setConfig(Configuration oConfig)
	{
		m_oConfig = oConfig;
	}
	
	/**
	 * Try to load a class-delegated configuration file from a path, overriding
	 * any loaded configuration before.
	 * 
	 * @param sPath path to a xml file containing the data of the config file
	 */
	public boolean loadConfig(String sPath)
	{
		if(m_cConfigCls == null)
			return false;
		
		File oFile = new File(sPath);
		if(oFile.isFile() && oFile.canRead())
		{
			try
			{
				Configuration newConfig = 
						(Configuration)m_cConfigCls.newInstance();
				FileInputStream stream = new FileInputStream(oFile);
				Configuration loadedConfig = (Configuration) m_cConfigCls.cast(
						getXStream().fromXML(stream));
				stream.close();
				
				newConfig.copyFrom(loadedConfig);
				m_oConfig = newConfig;
				m_oConfig.resetObservers();
				m_oConfig.setFile(oFile);
				return true;
			}
			catch (IOException |
				   InstantiationException |
				   IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	/**
	 * Wrapper for loadConfig and createConfig which calls createConfig if the
	 * loadConfig using the passed path fails
	 * 
	 * @param sPath path to a xml file containing the data of the config file
	 * @return true if the configuration object was loaded correctly
	 */
	public boolean loadOrCreateConfig(String sPath)
	{
		if(!loadConfig(sPath))
			return createConfig(sPath);
		return true;
	}
	
	/**
	 * Create a new configuration object using the class-delegate and overriding
	 * a previously created one
	 * 
	 * @param sPath path to the xml file which will contain the data of the 
	 * 		  config file
	 * @return true if the configuration object has been created,
	 * false otherwise
	 */
	private boolean createConfig(String sPath)
	{
		boolean bSuccess = false;
		if(m_cConfigCls != null)
		{
			try
			{
				m_oConfig = (Configuration) m_cConfigCls.newInstance();
				bSuccess = saveConfig(sPath);
			}
			catch (InstantiationException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}
		
		return bSuccess && m_oConfig != null;
	}
	
	/**
	 * Save the configuration to a xml file at the location to which the 
	 * configuration file was loaded, if it exists.
	 */
	public boolean saveConfig()
	{
		// TODO: Raise error if loaded path is null
		if(m_oConfig != null)
		{
			File oFile = m_oConfig.getFile();
			if(oFile != null)
				return saveConfig(oFile);
		}	
		return false;
	}

	/**
	 * Save the configuration to a xml file
	 * 
	 * @param sPath path to which the xml file is saved
	 * @return true if file was saved, false otherwise
	 */
	public boolean saveConfig(String sPath)
	{
		if(sPath != null && !sPath.isEmpty())
			return saveConfig(new File(sPath));
		return false;
	}
	
	/**
	 * Save the configuration to a xml file
	 * 
	 * @param oFile file in xml format to which the configuration is to be saved
	 * @return true if file was saved, false otherwise
	 */
	public boolean saveConfig(File oFile)
	{
		// TODO: Raise error if configuration instance is null
		if(m_oConfig == null)
			return false;
		
		if(!oFile.isFile())
		{
			try
			{
				File oParentDir = oFile.getParentFile();
				if(!oParentDir.isDirectory())
					oParentDir.mkdirs();
				
				oFile.createNewFile();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
				return false;
			}
		}
		try
		{
			FileOutputStream stream = new FileOutputStream(oFile);
			getXStream().toXML(m_oConfig, stream);
			stream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		m_oConfig.setFile(oFile);
		return true;
	}
}
