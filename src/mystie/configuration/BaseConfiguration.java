/**
 * Implementation of a configuration file using a hash-map with string key 
 * for data holding
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.configuration;

import java.io.File;
import java.util.HashMap;
import java.util.Observer;
import java.util.Set;

public abstract class BaseConfiguration implements Configuration
{
	File m_oSaveFile;
	
	protected HashMap<String, ConfigVariable> m_hmConfigVar = 
			new HashMap<String, ConfigVariable>();
	
	/**
	 * Resets the fields' value defined with a default value in the 
	 * m_hmDefaultFields hash-map
	 * <br><br>
	 * Implicitly, the default fields with their default values are added to
	 * the variables hash-map if they were not already. Therefore, this method
	 * can be used as an initializer.
	 */
	private void resetValuesToDefault()
	{
		for(ConfigVariable var: m_hmConfigVar.values())
			var.resetToDefault();
	}
	
	/**
	 * Add an object linked to the passed key string
	 * 
	 * @param key string key linked to the value
	 * @param val object linked to the key
	 */
	@Override
	public void addVar(String key, ConfigVariable var)
	{
		m_hmConfigVar.put(key, var);
	}
	
	/**
	 * Removes a key-object pair object linked to the passed key string
	 * 
	 * @param key string key linked to the value
	 * @return variable linked to the key, null if the key is not found
	 */
	@Override
	public ConfigVariable removeVar(String key)
	{
		return m_hmConfigVar.remove(key);
	}
	
	/**
	 * @param key string key linked to the value
	 * @return variable linked to the key, null if the key is not found
	 */
	@Override
	public ConfigVariable getVar(String key)
	{
		return m_hmConfigVar.get(key);
	}
	
	/**
	 * Get a set of keys to which variables currently held by the configuration
	 * object are bound
	 * 
	 * @return set of keys bound to configuration's variables
	 */
	public Set<String> getKeys()
	{
		return m_hmConfigVar.keySet();
	}
	
	/**
	 * Copy configuration variables from another configuration object, given
	 * that they are of the same type.
	 * 
	 * @param oConfig configuration object to copy variables from
	 * @return true if the copy was successful, false otherwise
	 */
	@Override
	public boolean copyFrom(Configuration oConfig)
	{
		if(oConfig == null ||
		   oConfig.getClass() != getClass())
			return false;
		
		for(String key : oConfig.getKeys())
		{
			ConfigVariable cvSrc = oConfig.getVar(key);
			if(cvSrc != null)
				addVar(key, cvSrc.duplicate());
		}
		
		return true;
	}
	
	/**
	 * Reset observers for all the variables to none.
	 */
	public void resetObservers()
	{
		for(ConfigVariable var: m_hmConfigVar.values())
			var.deleteObservers();
	}
	
	/**
	 * Set observer object argument as observer for all the variables
	 */
	public void observeAll(Observer obs)
	{
		for(ConfigVariable var: m_hmConfigVar.values())
			var.addObserver(obs);
	}
	
	/**
	 * @return file to which configuration is saved
	 */
	public File getFile()
	{
		return m_oSaveFile;
	}
	
	/**
	 * @param oSaveFile file to linked to the configuration
	 */
	public void setFile(File oSaveFile)
	{
		m_oSaveFile = oSaveFile;
	}
}
