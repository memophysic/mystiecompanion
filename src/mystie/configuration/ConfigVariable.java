/**
 * Configuration variable which is stored in a configuration object
 */

package mystie.configuration;

import java.util.Observable;

@SuppressWarnings("rawtypes")
public class ConfigVariable extends Observable
{
	private final Class m_clsType;
	private final String m_sKeyCode;
	private final String m_sLabel;
	private final Object m_oDefaultValue;
	private Object m_oValue;
	
	/**
	 * Construct a variable with a key code (must be unique), an object as a
	 * value, a default value if relevant and a label to represent it if 
	 * relevant
	 * 
	 * @param keyCode string code for storage (unique)
	 * @param type type of the variable
	 * @param val value of the variable
	 * @param defVal default value of the variable
	 * @param label string representation of the variable (not its value)
	 */
	public ConfigVariable(String keyCode, Class type, Object defVal, 
			String sLabel, Object val)
	{
		super();
		
		m_clsType = type;
		m_sKeyCode = keyCode;
		m_sLabel = sLabel;
		
		if(isObjectValid(defVal))
			m_oDefaultValue = defVal;
		else
			throw new IllegalArgumentException();
		
		try {
			setValue(val);
		} catch(IllegalArgumentException e) {
			setValue(defVal);
		}
	}
	
	public ConfigVariable(String keyCode, Class type, Object defVal, 
			String sLabel)
	{
		this(keyCode, type, defVal, sLabel, null);
		setValue(defVal);
	}
	
	/**
	 * @param val object to be tested
	 * @return true if the object of the correct type or null,
	 * 		   false otherwise
	 */
	private boolean isObjectValid(Object val)
	{
		return val == null || val.getClass() == m_clsType;
	}
	
	/**
	 * @return type of the variable
	 */
	public Class getType()
	{
		return m_clsType;
	}
	
	/**
	 * @return variable key code
	 */
	public String getKeyCode()
	{
		return m_sKeyCode;
	}
	
	/**
	 * @return variable label
	 */
	public String getLabel()
	{
		return m_sLabel;
	}
	
	/**
	 * @return default value object
	 */
	public Object getDefaultValue()
	{
		return m_oDefaultValue;
	}
	
	/**
	 * @return value object
	 */
	public Object getValue()
	{
		return m_oValue;
	}
	
	/**
	 * Reset the variable to the default value
	 */
	public void resetToDefault()
	{
		setValue(m_oDefaultValue);
	}
	
	/**
	 * Set the value of the variable
	 * @param val value of the variable to be set
	 * @param bNotifyObserver true to notify observers, false otherwise
	 */
	public void setValue(Object val, boolean bNotifyObserver)
	{
		if(isObjectValid(val))
		{
			if(m_oValue != val)
				setChanged();
			
			m_oValue = val;
			if(bNotifyObserver)
				notifyObservers();
		}
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Set the value of the variable, notify observers by default
	 * @param val value of the variable to be set
	 */
	public void setValue(Object val)
	{
		setValue(val, true);
	}
	
	public ConfigVariable duplicate() {
		return new ConfigVariable(
				getKeyCode(),
				getType(),
				getDefaultValue(),
				getLabel(),
				getValue());
	}
}
