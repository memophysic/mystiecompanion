/**
 * Interface to load, save and keep various configuration settings and variables
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.configuration;

import java.io.File;
import java.util.Observer;
import java.util.Set;

public interface Configuration
{
	// DB managing methods
	public void addVar(String key, ConfigVariable val);
	public ConfigVariable removeVar(String key);
	public ConfigVariable getVar(String key);
	public Set<String> getKeys();
	public boolean copyFrom(Configuration oConfig);
	
	// Observer pattern methods
	public void resetObservers();
	public void observeAll(Observer obs);
	
	// Saving methods
	public File getFile();
	public void setFile(File oSaveFile);
}
