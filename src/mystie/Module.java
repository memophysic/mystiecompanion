/**
 * High-level interface which allows flexibility over intialization and handling
 * of bigger chucks of objects in the application
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie;

import mystie.environment.Environment;

public interface Module
{
	/**
	 * Initialize the module
	 */
	public void initialize();
	
	/**
	 * @return parent environment to which the module is bound
	 */
	public Environment getEnvironment();
}
