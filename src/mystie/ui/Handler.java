/**
 * Interface for an handler, which can prompt the user to make decisions
 * and decide what to do depending on the type of parameter passed.
 * Every implementation will allow to contact the user from a way or another.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.ui;

import mystie.core.project.ProjectConfig;

public interface Handler
{
	/**
	 * Prompt the user for a yes/no question
	 * 
	 * @param oCaller caller object which called the handler
	 * @param sCallerDesc description of the caller
	 * @param sMsg question to display
	 * @return true if the answer is yes, false if no or cancelled
	 */
	public boolean askYesNo(Object oCaller, String sCallerDesc, String sMsg);
	
	/**
	 * Prompt the user to make a decision between three items
	 * 
	 * @param oCaller caller object which called the handler
	 * @param sCallerDesc description of the caller
	 * @param sMsg question to display
	 * @param options array of options to offer
	 * @return integer value corresponding to the index in the array of the 
	 * option chosen
	 */
	public int askThreeOptions(Object oCaller, String sCallerDesc, String sMsg,
			String[] options);
	
	/**
	 * Alert the user of a problem that has occurred
	 * 
	 * @param oCaller caller object which called the handler
	 * @param sCallerDesc description of the caller
	 * @param sMsg message to display
	 */
	public void alert(Object oCaller, String sCallerDesc, String sMsg);
	
	/**
	 * Inform the user of new information
	 * 
	 * @param oCaller caller object which called the handler
	 * @param sCallerDesc description of the caller
	 * @param sMsg message to display
	 */
	public void inform(Object oCaller, String sCallerDesc, String sMsg);
	
	/**
	 * Create new project parameters object using information from the user
	 * 
	 * @param oCaller caller object which called the handler
	 * @return project parameters or null if cancelled action
	 */
	public ProjectConfig createProjectConfig(Object oCaller);
}
