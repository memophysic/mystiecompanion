/**
 * Base handler for User-Interaction throughout the application.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.ui;

public abstract class BaseHandler implements Handler
{
	
}
