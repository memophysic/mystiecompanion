package mystie.core.project.actions;

import mystie.core.action.BaseActionRetriever;

public class PrjActions extends BaseActionRetriever
{
	public final static String KEY_PREFIX = "ACT_PRJ_";
	
	public final PrjCreateAndLoad ACT_CREATE_AND_LOAD = new PrjCreateAndLoad();
	public final static String KEY_CREATE_AND_LOAD = KEY_PREFIX + 
													 "CREATE_AND_LOAD";
	
	public final PrjOpen ACT_OPEN = new PrjOpen();
	public final static String KEY_OPEN = KEY_PREFIX + "OPEN";
	
	public final PrjSave ACT_SAVE = new PrjSave();
	public final static String KEY_SAVE = KEY_PREFIX + "SAVE";
	
	public final PrjClose ACT_CLOSE = new PrjClose();
	public final static String KEY_CLOSE = KEY_PREFIX + "CLOSE";
	
	public PrjActions()
	{
		m_hmActions.put(KEY_CREATE_AND_LOAD, ACT_CREATE_AND_LOAD);
		m_hmActions.put(KEY_OPEN, ACT_OPEN);
		m_hmActions.put(KEY_SAVE, ACT_SAVE);
		m_hmActions.put(KEY_CLOSE, ACT_CLOSE);
	}
}
