/**
 * Class for action for creating and loading a project into an environment's
 * workspace
 */

package mystie.core.project.actions;

import mystie.core.action.Action;
import mystie.core.project.Project;
import mystie.core.project.ProjectConfig;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;
import mystie.exceptions.BadProjectParametersException;

public class PrjCreateAndLoad implements Action
{
	
	/**
	 * @param args {ProjectConfig, Environment}
	 * @return 0 for success, -1 for error
	 */
	@Override
	public int execute(final Object[] args)
	{
		ProjectConfig prjParams = (ProjectConfig)args[0];
		if(prjParams == null)
			return -1;
		
		try
		{
			Project oPrj = new Project(prjParams);
			Environment env = ((Environment)args[1]);
			if(env == null)
				return -1;
			Workspace wksp = env.getWorkspace();
			assert (wksp != null);
			wksp.loadProject(oPrj);
		}
		catch (BadProjectParametersException exp)
		{
			System.out.println("Error: " + exp.toString());
			Environment env = ((Environment)args[1]);
			if(env != null)
				env.getHandler().alert(this, "New Project",
						exp.toString());
			return -1;
		}
		
		return 0;
	}
}
