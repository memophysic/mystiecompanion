package mystie.core.project.actions;

import mystie.core.action.Action;
import mystie.core.project.Project;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;

public class PrjSave implements Action
{
	/**
	 * args {Environment}
	 */
	public int execute(final Object[] args)
	{
		Environment env = (Environment)args[0];
		assert(env != null);
		
		Workspace wspc = env.getWorkspace();
		assert(wspc != null);
		
		Project prj = wspc.getProject();
		if(prj != null)
			return prj.saveProject() ? 0: -1;
		else
			return -1;
	}
}
