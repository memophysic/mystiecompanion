package mystie.core.project.actions;

import java.io.File;

import mystie.core.action.Action;
import mystie.core.project.Project;
import mystie.environment.Environment;
import mystie.exceptions.BadProjectParametersException;

public class PrjOpen implements Action
{
	/**
	 * @param args {Environment, File} where File is project directory
	 */
	@Override
	public int execute(Object[] args)
	{
		Environment env = (Environment)args[0];
		assert(env != null);
		
		File oPrjDir = (File)args[1];
		if(oPrjDir != null)
		{
			File oPrjCfg = new File(
					oPrjDir.getPath() + "\\" + Project.FILENAME);
			
			if(oPrjCfg != null && oPrjCfg.isFile() && oPrjCfg.canRead())
				return loadProject(env, oPrjDir) ? 0: -1;
		}
		
		return -1;
	}
	
	/**
	 * Load a project using a project file
	 * 
	 * @param env Environment in which the project should be loaded
	 * @param oPrjDir Project directory as file which must exist
	 * @return true if the project was loaded successfully, false otherwise
	 */
	public boolean loadProject(Environment env, File oPrjDir)
	{
		try
		{
			Project oPrj = new Project(oPrjDir.getPath());
			if(env.getWorkspace().loadProject(oPrj))
				env.getPresenter().presentProject();
			else
				return false;
		}
		catch (BadProjectParametersException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
