package mystie.core.project.actions;

import mystie.core.action.Action;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;

public class PrjClose implements Action
{
	/**
	 * args {Environment}
	 */
	public int execute(final Object[] args)
	{
		Environment env = (Environment)args[0];
		assert(env != null);
		
		Workspace wspc = env.getWorkspace();
		assert(wspc != null);
		
		return wspc.unloadProject(true) ? 0: -1;
	}
}
