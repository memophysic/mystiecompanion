/**
 * Class defining the project's parameters such as title, definition, 
 * paths, etc.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.core.project;

import java.io.File;
import java.util.HashMap;

import mystie.configuration.BaseConfiguration;
import mystie.configuration.ConfigVariable;
import mystie.exceptions.BadProjectParametersException;

public class ProjectConfig extends BaseConfiguration
{
	// Key codes for variables
	public static final String KEY_TITLE = "PRJPRM_PRJ_TITLE";
	public static final String DVL_TITLE = "New Project";
	public static final String LBL_TITLE = "Project Title";
	
	public static final String KEY_AUTHOR = "PRJPRM_PRJ_AUTHOR";
	public static final String DVL_AUTHOR = "Unknown Author";
	public static final String LBL_AUTHOR = "Project Author";
	
	public static final String KEY_VERID = "PRJPRM_PRJ_VERID";
	public static final Long   DVL_VERID = (long) 0;
	public static final String LBL_VERID = "Version ID";
	
	public static final String[] FIELD_KEYS = 
		{KEY_TITLE, KEY_AUTHOR, KEY_VERID};
	
	public HashMap<String, Object> m_hmProjectVariables = 
			new HashMap<String, Object>();
	
	public ProjectConfig()
	{
		m_hmConfigVar.put(KEY_TITLE, new ConfigVariable(
				KEY_TITLE, String.class, DVL_TITLE, LBL_TITLE));
		
		m_hmConfigVar.put(KEY_AUTHOR, new ConfigVariable(
				KEY_AUTHOR, String.class, DVL_AUTHOR, LBL_AUTHOR));
		
		m_hmConfigVar.put(KEY_VERID, new ConfigVariable(
				KEY_VERID, Long.class, DVL_VERID, LBL_VERID));
	}
	
	/**
	 * 
	 * @param sTitle title of the project
	 * @param sAuthor author of the project
	 * @param oPrjFile project folder file
	 * @param nVerID version number of the project
	 */
	public ProjectConfig(String sTitle, String sAuthor, File oPrjFile,
			long nVerID)
	{
		this();
		
		setTitle(sTitle);
		setAuthor(sAuthor);
		setFile(oPrjFile);
		setVersionID(nVerID);
	}
	
	public void setTitle(String title)
	{
		getVar(KEY_TITLE).setValue(title);
	}
	
	public String getTitle()
	{
		return (String) getVar(KEY_TITLE).getValue();
	}
	
	public void setAuthor(String def)
	{
		getVar(KEY_AUTHOR).setValue(def);
	}
	
	public String getAuthor()
	{
		return (String) getVar(KEY_AUTHOR).getValue();
	}
	
	public void setVersionID(Long vID)
	{
		getVar(KEY_VERID).setValue(vID);
	}
	
	public Long getVersionID()
	{
		return (Long) getVar(KEY_VERID).getValue();
	}
	
	// ------------------------------------------------------------------------
	// -- Static methods
	// ------------------------------------------------------------------------
	
	public static void bValidate(ProjectConfig oPrjConfig)
			throws BadProjectParametersException
	{
		// TODO: Validate project parameters
		/*if(false)
			throw new BadProjectParametersException(
					"Validation for the project parameters has failed.");*/
	}
}
