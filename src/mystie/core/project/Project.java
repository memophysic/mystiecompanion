/**
 * Class defining the project's data and structure
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.core.project;

//import java.io.File;

import java.io.File;
import java.io.IOException;

import mystie.configuration.ConfigManager;
import mystie.core.project.content.ContentHierarchy;
import mystie.exceptions.BadProjectParametersException;

public class Project
{
	public final static String FILENAME = "prjConfig.xml";
	
	private ConfigManager m_oConfigMng;
	private int m_nUnsavedChanges = 0;
	
	/**
	 * Project constructor which initializes its own parameters to default ones
	 * 
	 * @param sPrjDir load/save directory for the project
	 * @throws BadProjectParametersException is thrown if the passed parameters
	 * are found to be invalid.
	 */
	public Project(String sPrjDir)
			throws BadProjectParametersException
	{
		m_oConfigMng = new ConfigManager();
		m_oConfigMng.initializeConfig(ProjectConfig.class,
									  sPrjDir + File.separator + FILENAME);
		
		ProjectConfig.bValidate((ProjectConfig)m_oConfigMng.getConfig());
		
		createPrjHierarchy();
	}
	
	/**
	 * Project constructor which takes an already initialized configuration
	 * and uses it as-is
	 * 
	 * @param oPrjConfig already initialized project parameters object
	 * @throws BadProjectParametersException is thrown if the passed parameters
	 * are found to be invalid.
	 */
	public Project(ProjectConfig oPrjConfig)
			throws BadProjectParametersException
	{
		ProjectConfig.bValidate(oPrjConfig);
		
		m_oConfigMng = new ConfigManager();
		m_oConfigMng.setConfig(oPrjConfig);
		
		saveProject();
		createPrjHierarchy();
	}
	
	// -- Getters
	/**
	 * Get the project parameters as an object
	 * 
	 * @return project parameters
	 */
	public final ProjectConfig getConfig()
	{
		return (ProjectConfig)m_oConfigMng.getConfig();
	}
	
	// -- Setters
	
	
	// -- Is/Has
	
	/**
	 * @return true if unsaved changes exist, false otherwise
	 */
	public boolean hasUnsavedChanges()
	{
		return m_nUnsavedChanges > 0;
	}
	
	// -- General
	
	/**
	 * Create the project hierarchy for the current project
	 * The configuration for the project must exist and have been initialized
	 * @return true if successful, false otherwise
	 */
	private boolean createPrjHierarchy()
	{
		try
		{
			ContentHierarchy.createProjectHierarchy(getProjectRoot());
			return true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean saveProject()
	{
		return m_oConfigMng.saveConfig();
	}
	
	public File getProjectRoot()
	{
		File oPrjFile = getConfig().getFile();
		if(oPrjFile != null)
		{
			File oPrjDir = oPrjFile.getParentFile();
			return oPrjDir.isDirectory() ? oPrjDir : null;
		}
		return null;
	}
}