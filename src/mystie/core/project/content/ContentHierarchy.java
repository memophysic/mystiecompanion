/**
 * Create and manages the content (files and metadata) hierarchy for a project
 */

package mystie.core.project.content;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import mystie.core.FileTools;
import mystie.core.FileTools.FileExtFilter;
import mystie.core.FileTools.FileDirFilter;

public class ContentHierarchy
{	
	public static final String DIRNAME_OVERWORLD = "overworld";
	public static final String DIRNAME_ITEM = "item";
	public static final String DIRNAME_NPC = 
			DIRNAME_ITEM + File.separator + "npc";
	public static final String DIRNAME_FRIENDLY = 
			DIRNAME_NPC + File.separator + "friendly";
	public static final String DIRNAME_HOSTILE = 
			DIRNAME_NPC + File.separator + "hostile";
	public static final String DIRNAME_TILE = 
			DIRNAME_ITEM + File.separator + "tile";
	public static final String DIRNAME_MENU = "menu";
	
	public static final String FILEEXT_MAP = "map";
	
	public final static String[] DIRECTORIES = {
		DIRNAME_OVERWORLD,
		DIRNAME_ITEM,
			DIRNAME_NPC,
				DIRNAME_FRIENDLY,
				DIRNAME_HOSTILE, 
			DIRNAME_TILE,
		DIRNAME_MENU,
		};
	
	/**
	 * @param oDir root directory
	 * @param subDir subdirectory path relative to the root directory
	 * @return a valid (existing) target directory, null if unsuccessful
	 */
	private static File getTargetDir(File oRootDir, String subDir)
	{
		File targetDir = subDir != null ?
				new File(oRootDir.getPath() + File.separator + subDir) : oRootDir;
				
		return FileTools.bValidateDirectory(targetDir) ? targetDir : null;
	}
	
	/**
	 * @param oRootDir non-null root directory
	 * @param subDir subdirectory of the project, can be given by an entry from
	 * ContentHierarchy.DIRECTORIES
	 * @param filter filter used in the listing of files
	 * @return array of File that have the passed extension
	 */
	public static File[] getFileList(File oRootDir, String subDir,
			FileFilter filter)
	{
		File targetDir = getTargetDir(oRootDir, subDir);
		if(targetDir == null)
			return null;
		
		return targetDir.listFiles(filter);
	}
	
	/**
	 * @param oRootDir non-null root directory
	 * @param subDir subdirectory of the project, can be given by an entry from
	 * ContentHierarchy.DIRECTORIES
	 * @param sExt file extension to filter with
	 * @return array of File that have the passed extension
	 */
	public static File[] getFileList(File oRootDir, String subDir, String sExt)
	{
		return getFileList(oRootDir, subDir, new FileExtFilter(sExt));
	}
	
	/**
	 * @param oRootDir non-null root directory
	 * @param subDir subdirectory of the project, can be given by an entry from
	 * ContentHierarchy.DIRECTORIES
	 * @return array of File representing directories
	 */
	public static File[] getDirList(File oRootDir, String subDir)
	{
		return getFileList(oRootDir, subDir, new FileDirFilter());
	}
	
	
	/**
	 * Create a new directory in a subdirectory of the project root
	 * 
	 * @param oPrjRoot file representing the project root directory
	 * @param sSubPath subpath to the new directory from the root directory
	 * (can use an entry in ContentHierarchy.DIRECTORIES), null if the new
	 * directory is to be in the root folder
	 * @param sDirName the new directory's name
	 * @return true if creation is successful, false otherwise
	 */
	public static boolean createSubDir(File oPrjRoot, String sSubPath, 
			String sDirName)
	{
		if(!FileTools.bValidateDirectory(oPrjRoot))
			return false;
		
		String sRootPath = sSubPath != null ?
			oPrjRoot.getPath() + File.separator + sSubPath : oPrjRoot.getPath();
		
		return FileTools.createDir(sRootPath, sDirName);
	}
	
	/**
	 * Create project file hierarchy in the passed project root directory
	 * @param oPrjRoot directory where to create the project hierarchy,
	 * typically a project root directory
	 * @return true if the creation was a success, false otherwise
	 */
	public static boolean createProjectHierarchy(File oPrjRoot)
			throws IOException
	{
		if(!FileTools.bValidateDirectory(oPrjRoot))
			return false;
		
		String sRootPath = oPrjRoot.getPath();
		
		boolean bSuccess = false;
		for(String dirName : DIRECTORIES)
		{
			bSuccess = FileTools.createDir(sRootPath, dirName);
			if(!bSuccess)
				break;
		}
		
		return bSuccess;
	}
}
