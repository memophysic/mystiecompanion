/**
 * Regroup different tool functions to deal with files
 */

package mystie.core;

import java.io.File;
import java.io.FileFilter;

import org.apache.commons.io.FilenameUtils;

public class FileTools
{	
	/*
	 * 
	 */
	public static boolean createDir(String sRootPath, String sDirName, 
			boolean bAllowSubDirCreation)
	{		
		File dir = new File(sRootPath + File.separator + sDirName);
		return bAllowSubDirCreation ? dir.mkdirs() : dir.mkdir();
	}
	
	public static boolean createDir(String sRootPath, String sDirName)
	{
		return createDir(sRootPath, sDirName, false);
	}
	
	/**
	 * @param oFile file to validate
	 * @return true if the file exist and is a directory, false otherwise
	 */
	public static boolean bValidateDirectory(File oFile)
	{
		if(oFile == null)
			return false;
		return oFile.isDirectory();
	}
	
	public static String getHomePath()
	{
		return System.getProperty("user.home");
	}
	
	public static String getMystieHomePath()
	{
		return getHomePath() + File.separator + ".mystieCo";
	}
	
	/**
	 * File filter accepting files based on their extension
	 */
	public static class FileExtFilter implements FileFilter
	{
		String m_sExt;
		
		public FileExtFilter(String sExt)
		{
			m_sExt = sExt;
		}
		
		@Override
		public boolean accept(File oFile)
		{
			String sExt = FilenameUtils.getExtension(oFile.getName());
			return sExt.compareToIgnoreCase(m_sExt) == 0;
		}
	}
	
	/**
	 * File filter accepting files based on their extension
	 */
	public static class FileDirFilter implements FileFilter
	{
		@Override
		public boolean accept(File oFile)
		{
			return oFile.isDirectory();
		}
	}
	
}
