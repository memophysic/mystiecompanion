/**
 * Interface for a workspace which manages a project's data
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.core.workspace;

import mystie.Module;
import mystie.core.project.Project;

public interface Workspace extends Module
{
	public boolean loadProject(Project oPrj);
	public boolean unloadProject(boolean bAskSave);
	public Project getProject();
	public boolean hasProject();
}
