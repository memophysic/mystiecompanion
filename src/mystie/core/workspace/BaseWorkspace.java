/**
 * Base class for the workspace
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.core.workspace;

import mystie.BaseModule;
import mystie.core.project.Project;
import mystie.environment.Environment;

public class BaseWorkspace extends BaseModule implements Workspace
{
	private String m_sDesc = "BaseWorkspace";
	private static Project m_oProject;
	
	public BaseWorkspace(Environment env)
	{
		super(env);
	}
	
	@Override
	public void initialize()
	{
		
	}
	
	/**
	 * Load a project from a project object, unloading any previous set project
	 * 
	 * @param oPrj project object to be set
	 */
	public boolean loadProject(Project oPrj)
	{
		boolean bSuccess = unloadProject(true);
		if(!bSuccess)
			return false;
		
		setProject(oPrj);
		getEnvironment().setLastProjectLoaded(oPrj.getProjectRoot());
		
		return true;
	}
	
	/**
	 * Unload any loaded project
	 * 
	 * @param bAskSave ask to save the project before unloading if true,
	 * doesn't ask if false
	 */
	public boolean unloadProject(boolean bAskSave)
	{
		if(m_oProject == null)
			return true;
		
		if(bAskSave && m_oProject.hasUnsavedChanges())
			if(getEnvironment().getHandler().askYesNo(this, m_sDesc,
					"The project has unsaved changes, would you like to save?"))
				if(!m_oProject.saveProject())
					return false;
		
		getEnvironment().setLastProjectLoaded(null);
		m_oProject = null;
		
		return true;
	}
	
	@Override
	public boolean hasProject()
	{
		return m_oProject != null;
	}
	
	// -- Getters
	
	/**
	 * Get the currently loaded project
	 */
	public Project getProject()
	{
		return m_oProject;
	}
	
	// -- Setters
	
	private void setProject(Project newProject)
	{
		m_oProject = newProject;
	}
}
