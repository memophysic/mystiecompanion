package mystie.core.action;

public interface Action
{
	/**
	 * Execute a core action
	 * 
	 * @param args arguments for the action in question
	 * @return 0 for success, otherwise implementation specific error code
	 */
	public int execute(final Object[] args);
}