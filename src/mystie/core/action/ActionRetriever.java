package mystie.core.action;

public interface ActionRetriever
{
	public Action getAction(String actionKeyCode);
}
