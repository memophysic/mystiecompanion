package mystie.core.action;

import java.util.HashMap;

public class BaseActionRetriever implements ActionRetriever
{
	protected HashMap<String, Action> m_hmActions = 
			new HashMap<String, Action>();
	
	public Action getAction(String actionKey)
	{
		return m_hmActions.get(actionKey);
	}
}
