/**
 * Base class for the presenter
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie;

import mystie.configuration.ConfigManager;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;
import mystie.ui.Handler;

public abstract class BasePresenter extends BaseModule implements Presenter
{
	final private Handler m_oHandler;
	final private AppSetup m_oAppSetup;
	final private ConfigManager m_oConfMng;
	
	public BasePresenter(Environment env, Handler oHandler, AppSetup oAppSetup)
	{
		super(env);
		m_oHandler = oHandler;
		m_oAppSetup = oAppSetup;
		m_oConfMng = new ConfigManager();
	}
	
	@Override
	public ConfigManager getConfigMng()
	{
		return m_oConfMng;
	}
	
	@Override
	public Workspace getWorkspace()
	{
		return getEnvironment().getWorkspace();
	}
	
	@Override
	public Handler getHandler()
	{
		return m_oHandler;
	}
	
	@Override
	public AppSetup getAppSetup()
	{
		return m_oAppSetup;
	}
}
