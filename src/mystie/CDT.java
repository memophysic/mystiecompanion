/**
 * Set of variables and functions used only for the development of the 
 * companion application, notably a way to easily know if the application runs
 * in debug mode or not.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie;

import java.lang.management.ManagementFactory;

public class CDT
{
	public final static boolean DEBUG = isDebug();
	private static boolean isDebug()
	{
		return ManagementFactory.getRuntimeMXBean().
				getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	}
}
