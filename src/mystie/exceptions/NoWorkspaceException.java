/**
 * Exception for no workspace referenced when attempting to retrieve it.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.exceptions;

@SuppressWarnings("serial")
public class NoWorkspaceException extends Exception
{
	public NoWorkspaceException(String message)
	{
        super(message);
    }
	
	public NoWorkspaceException(Throwable throwable)
	{
		super(throwable);
	}

    public NoWorkspaceException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
