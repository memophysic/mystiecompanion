/**
 * Exception for invalid project parameters
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.exceptions;

@SuppressWarnings("serial")
public class BadProjectParametersException extends Exception
{
	public BadProjectParametersException(String message)
	{
        super(message);
    }
	
	public BadProjectParametersException(Throwable throwable)
	{
		super(throwable);
	}

    public BadProjectParametersException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
