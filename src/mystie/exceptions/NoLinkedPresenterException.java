/**
 * Exception for no linked presenter to a sub-presenter class attempting to 
 * reach its parent presenter.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.exceptions;

@SuppressWarnings("serial")
public class NoLinkedPresenterException extends Exception
{
	public NoLinkedPresenterException(String message)
	{
        super(message);
    }
	
	public NoLinkedPresenterException(Throwable throwable)
	{
		super(throwable);
	}

    public NoLinkedPresenterException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
