/**
 * Exception for unfound environment object or reference
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.exceptions;

@SuppressWarnings("serial")
public class NoSuchEnvironmentException extends Exception
{
	public NoSuchEnvironmentException(String message)
	{
        super(message);
    }
	
	public NoSuchEnvironmentException(Throwable throwable)
	{
		super(throwable);
	}

    public NoSuchEnvironmentException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}