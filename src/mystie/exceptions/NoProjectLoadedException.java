/**
 * Exception for no project loaded when attempting an operation on the current
 * project loaded.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie.exceptions;

@SuppressWarnings("serial")
public class NoProjectLoadedException extends Exception
{
	public NoProjectLoadedException(String message)
	{
        super(message);
    }
	
	public NoProjectLoadedException(Throwable throwable)
	{
		super(throwable);
	}

    public NoProjectLoadedException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}