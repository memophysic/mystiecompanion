package mystie;

public abstract class BaseAppSetup implements AppSetup
{
	public abstract void setupAll();
}
