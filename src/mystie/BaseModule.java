package mystie;

import mystie.environment.Environment;

public abstract class BaseModule implements Module
{
	final Environment m_oEnvironment;
	
	public BaseModule(Environment env)
	{
		assert(env != null);
		m_oEnvironment = env;
	}
	
	public Environment getEnvironment()
	{
		return m_oEnvironment;
	}
}
