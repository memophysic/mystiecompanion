/**
 * Abstract class which keeps references to the main components of information
 * representation to the user.
 * <br>
 * It is the creator's responsibility to setup the presenter such that it
 * has all the necessary references to accomplish its job.
 * 
 * @author		Samuel Longchamps
 * @version		%I%, %G%
 * @since		1.0
 */

package mystie;

import java.util.Observer;

import mystie.configuration.ConfigManager;
import mystie.core.workspace.Workspace;
import mystie.environment.Environment;
import mystie.ui.Handler;

public interface Presenter extends Observer, Module
{
	/**
	 * Present loading for the application's startup
	 * @param start True if the application starts loading, false if it ends
	 */
	public void presentInitializationLoading(boolean start);
	
	/**
	 * @return parent environment to which the presenter instance is bound
	 */
	public Environment getEnvironment();
	
	/**
	 * Get the workspace reference used by the presenter
	 * @return Workspace object reference
	 */
	public Workspace getWorkspace();
	
	/**
	 * Get the handler appropriate to the presenter
	 * @return handler object
	 */
	public Handler getHandler();
	
	/**
	 * Get the application setup object
	 * @return application setup object
	 */
	public AppSetup getAppSetup();
	
	/**
	 * Get the configuration manager object
	 * @return configuration manager object
	 */
	public ConfigManager getConfigMng();
	
	/**
	 * Present the project loaded in the workspace
	 */
	public void presentProject();
}
